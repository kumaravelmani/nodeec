ECHO "Starting the application"
CALL pm2 stop config/ecosystemb2b.config.js
CALL pm2 delete EasyConnectB2BApp
CALL git stash
CALL git pull
CALL npm install
CALL pm2 start config/ecosystemb2b.config.js --env production && PAUSE
