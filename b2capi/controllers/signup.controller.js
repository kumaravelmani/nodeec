const loginService = require('../services/loginService');
const logger = require('../utilities/logger');
const mailService = require('../services/MailServices');
const signupModule = {
    doSignup: async (req, res, next) => {
        try {
            const logObj = req.body;

            const response = await loginService.doSignUp(logObj);
            const responseData = response.data;
            if (responseData.result[0].error) {
                res.status(500).send(responseData.result[0].error);
            } else {
                res.status(200).send(responseData.result[0].message[0]);
            }
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    sendResetLink: async (req, res, next) => {
        const userName = req.query.userName;
        const accessToken = Buffer.from(userName, 'binary').toString('base64');
        logger.info(`User:: ${userName} accessToken: ${accessToken}`);
        loginService.validateUsername(userName).then(
            result => {
                const resultData = JSON.parse(result);
                if (resultData.result[0].error) {
                    res.status(500).send(resultData.result[0].error);
                } else {
                    const emailId = resultData.result[0].row[0].email;
                    mailService.sendEmail(accessToken, emailId)
                        .then(
                            response => {
                                const responseData = response;

                                const responseMessage = {
                                    "status":false,
                                    "messageId":''
                                }
                                const messageResponseCode = responseData.response;
                                if(messageResponseCode.indexOf('250 OK')) {
                                    responseMessage.status = true;
                                    responseMessage.messageId = responseData.messageId;
                                }
                                res.status(200).send(messageResponseCode);
                            }
                        )
                        .catch(
                            reason => {
                                res.status(500).send(reason);
                            }
                        );
                }
            }
        )
            .catch(
                reason => {
                    res.status(500).send(reason);
                }
            );
    },
    resetPassword: async (req, res, next) => {
        const resetColumnObj = req.body;
        loginService.resetPassword(resetColumnObj)
            .then(
                response => {
                    const resultData = JSON.parse(response);
                    if (resultData.result[0].error) {
                        res.status(500).send(resultData.result[0].error);
                    }
                    else {
                        res.status(200).send(resultData.result[0].message[0]);
                    }
                }
            )
            .catch(
                reason => {
                    res.status(500).send(reason);
                }
            )
    }
}

module.exports = signupModule;
