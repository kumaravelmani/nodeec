ECHO "Starting the Application"
CALL pm2 stop config/ecosystem.config.js
CALL pm2 delete EasyConnectB2CApp
CALL git stash
CALL git pull
CALL npm install
CALL pm2 start config/ecosystem.config.js --env production && PAUSE

