module.exports = {
    apps : [
        {
            name: "EasyConnectB2BApp",
            script: "./app/index.js",
            watch: true,
            instances: 2,
            autorestart: true,
            max_memory_restart: '1G',
            env: {
                "PORT": 3018,
                "NODE_ENV": "development",
                "API_URL": "http://192.168.12.8/mylabconnectx7scripts/",
                "DIR_PATH":"d:\\easyfiles\\",
                "DB_NAME": "easyconnect",
                "DIR_VIR_PATH": "http://192.168.12.8:3021/",
                "FTP_SERVER_IP": "129.213.34.178",
                "FTP_USERNAME":"opc",
                "FTP_UPLOAD_DIR":"/home/opc/easydent/",
                "RESET_LINK":"http://192.168.12.8:3022/resetPassword?sessionId=",
                "APP_TYPE": "b2b",
                "USERNAME":"apiadmin",
                "PASSWORD":"apiadmin@123",
                "ACCESS_KEY":"PT75SZ0M4KUWUGOKLLE9",
                "SECRET_KEY":"eBMn6KWWph8ZjcIOHQDZ7uVe5BmvQ7eDc6wBHCP7",
                "SERVER_NAME": "s3.us-east-1.wasabisys.com",
                "BUCKET_NAME": "easydentconnect",
                "S3_REGION": "us-east-1"
            },
            env_production: {
                "PORT": 3018,
                "NODE_ENV": "production",
                "API_URL": "http://localhost:3022/EasyConnectScripts/",
                "DIR_PATH":"D:\\AgileHealth\\AxpertWeb\\MyLabConnectDocs\\EasyConnectFiles\\",
                "DIR_VIR_PATH": "https://heartland.mylabconnect.com:3019",
                "FTP_SERVER_IP": "129.213.34.178",
                "FTP_USERNAME":"opc",
                "FTP_UPLOAD_DIR":"/home/opc/easydent/",
                "RESET_LINK":"https://heartland.mylabconnect.com:3022/resetPassword?sessionId=",
                "APP_TYPE": "b2b",
                "USERNAME":"apiadmin",
                "PASSWORD":"apiadmin@123",
                "ACCESS_KEY":"PT75SZ0M4KUWUGOKLLE9",
                "SECRET_KEY":"eBMn6KWWph8ZjcIOHQDZ7uVe5BmvQ7eDc6wBHCP7",
                "SERVER_NAME": "s3.us-east-1.wasabisys.com",
                "BUCKET_NAME": "easydentconnect",
                "S3_REGION": "us-east-1"
            }
        }
    ]
}
