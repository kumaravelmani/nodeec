const nodemailer = require("nodemailer");
const logger = require('../utilities/logger');

const mailConfig = {
    host: "sg2plcpnl0036.prod.sin2.secureserver.net",
    port: 587,
    secure: false, // true for 465, false for other ports
    auth: {
        user: "arutprakash@isithora.com", // generated ethereal user
        pass: "a1113" // generated ethereal password
    }
};

function constructHTMLString(accessToken) {
    let htmlString = `
            <html>
            <head>
            <link rel="stylesheet" href="https://maxcdn.bootstrapcdn.com/bootstrap/4.0.0/css/bootstrap.min.css" integrity="sha384-Gn5384xqQ1aoWXA+058RXPxPg6fy4IWvTNh0E263XmFcJlSAwiGgFAW/dAiS6JXm" crossorigin="anonymous">
        </head>
        <body> 
           <div class="card text-center">
          <div class="card-header">
            Reset Password
          </div>
          <div class="card-body">
            <h5 class="card-title">Use below Link to reset your password</h5>
            <p class="card-text">${process.env.RESET_LINK}${accessToken}</p>
            <p class="card-text">OR</p>
            <a href="${process.env.RESET_LINK}${accessToken}" class="btn btn-primary">Reset Password</a>
          </div>
          <div class="card-footer text-muted">
            Valid for 24 hours
          </div>
        </div>
        </body>
        </html>`;
    return htmlString;
}

async function sendEmail(accessToken, mailTo) {

    logger.info(`Mail Config::: ${mailConfig}`);
    let transporter = nodemailer.createTransport(mailConfig);

    return new Promise((resolve, reject) => {
        transporter.sendMail({
            from: '"Arut Prakash 👻" <arutprakash@isithora.com>', // sender address
            to: mailTo, // list of receivers
            subject: "Password Reset Link ✔", // Subject line
            text: "Hello world?", // plain text body
            html: constructHTMLString(accessToken) // html body
        }).then(
            response => {
                logger.info(`Mail Sent Response::: ${response}`);
                return resolve(response);
            }
        )
            .catch(
                reason => {
                    reject(reason);
                }
            )
    });

    logger.info(`Message sent: ${info.messageId}`);
    // Message sent: <b658f8ca-6296-ccf4-8306-87d57a0b4321@example.com>

}

module.exports = MailService = {
    sendEmail
};
