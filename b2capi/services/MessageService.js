const logger = require('../utilities/logger');
const axios = require('axios');
const CircularJSON = require('circular-json');
const commonService = require('./commonServices');

async function sendMessage(messageObj, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = messageObj.columns;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    if (messageObj.attachments.length > 0) {
        const axp_recid2 = [];
        let count =1;
        messageObj.attachments.forEach(
            value => {
                const axpRec2 = {
                    'rowno': `00${count}`,
                    'text': '0',
                    'columns': {}
                };
                axpRec2.columns = value;
                axp_recid2.push(axpRec2);
                count++;
            }
        );
        const recData2 = {};
        recData2['axp_recid2'] = axp_recid2;
        recdata.push(recData2);
    }
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'msgs',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }

    savedata = commonService.changeTransData(savedata);

    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Send Message paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Send Message Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}
async function getOrderMessage(orderParam, sessionId) {
    let getiView = {
        'name': 'gordmsg',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': orderParam
    };

    getiView = commonService.changeTransData(getiView);

    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Get Order Message Final paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}
module.exports = MessageService = {
    sendMessage,
    getOrderMessage
};
