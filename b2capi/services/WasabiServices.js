const fs = require('fs');
const path = require('path');
const S3 = require('aws-sdk/clients/s3');
const AWS = require('aws-sdk');
const logger = require('../utilities/logger');
const commonService = require('../services/commonServices');
const JSZip = require('jszip');
const wasabiEndpoint = new AWS.Endpoint(process.env.SERVER_NAME);
const accessKeyId = process.env.ACCESS_KEY;
const secretAccessKey = process.env.SECRET_KEY;
const bucketName = process.env.BUCKET_NAME;

const async = require('async');

const s3 = new S3({
    endpoint: wasabiEndpoint,
    region: process.env.S3_REGION,
    accessKeyId,
    secretAccessKey
});

async function uploadFile(remotePath, fileName) {

    return new Promise((resolve, reject) => {
        const params = {
            Bucket: bucketName,
            Key: fileName,
            Body: fs.readFileSync(remotePath)
        };

        s3.upload(params, function (s3Err, data) {
            if (s3Err) {
                logger.error(`Error WASABI ==> ${s3Err}`);
                return reject(s3Err);
            }
            console.log(`File uploaded successfully at ${data.Location}`)
            return resolve(data);
        });
    });
}

async function downloadFile(downloadLocation, s3Filepath) {
    return new Promise((resolve, reject) => {

        const params = {
            Bucket: bucketName,
            Key: s3Filepath
        };
        s3.getObject(params, (err, data) => {
            if (err) {
                logger.error(`Wasabi Download Error`);
                reject(err);
            }
            fs.writeFileSync(downloadLocation, data);
        });
    });
}

async function getSignedUrl(s3FilePath) {

    return new Promise((resolve, reject) => {
        s3.getSignedUrl('getObject', {
            Expires: 300,
            Bucket: bucketName,
            Key: s3FilePath
        }, function (err, url) {
            if (err) {
                logger.error(`Error: ${err}`);
                return reject(err);
            }
            logger.info(`URL is ${url}`);
            return resolve(url);
        });
    });
}
async function downloadZipFile(arrFilesObj) {
    const zip = new JSZip();
    const curDate = new Date();
    const curTimeStamp = `${curDate.getFullYear()}${commonService.zeroFill(curDate.getMonth() + 1)}${commonService.zeroFill(curDate.getDate())}_${commonService.zeroFill(curDate.getHours())}${commonService.zeroFill(curDate.getMinutes())}${commonService.zeroFill(curDate.getSeconds())}`;

    return new Promise((resolve, reject) => {
        async.eachLimit(arrFilesObj, 10, function(fileObj, next) {
            const params = {
                Bucket: bucketName,
                Key: fileObj.key
            };
            s3.getObject(params, function(err, data) {
                if (err) {
                    logger.error(`get image files ${err}`);
                } else {
                    logger.info(`Files Object ::: ${fileObj}`);
                    zip.file(arrFilesObj.indexOf(fileObj) + fileObj.name, data.Body);
                    next();
                }
            });
        }, function(err) {
            if (err) {
                logger.error(`Get Object Error :: ${err}`);
                return reject(err);
            } else {
                const content = zip.generate({
                    type: 'nodebuffer'
                });
                const params = {
                    Bucket: bucketName, // name of dest bucket
                    Key: `downloads/orders_${curTimeStamp}.zip`,
                    Body: content
                };
                s3.putObject(params, function(err, data) {
                    if (err) {
                        logger.error(`Convertion Error ${err}`); // an error occurred
                        return reject(err);
                    } else {
                        logger.info(`Uploaded data : ${JSON.stringify(data)}`);
                        return resolve(`downloads/orders_${curTimeStamp}.zip`);
                    }
                });
            }
        });
    });
}
module.exports = WasabiServices = {
    uploadFile,
    downloadFile,
    getSignedUrl,
    downloadZipFile
};
