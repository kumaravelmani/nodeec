const axios = require('axios');
const logger = require('../utilities/logger');
const fs = require("fs");
const md5 = require('md5');
const CircularJSON = require('circular-json');
const appType = process.env.APP_TYPE;
async function loadOrderData(columnObj, userCode, sessionId) {
    let getiView = {
        'name': '',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {}
    };
    getiView = changeTransData(getiView);

    getiView.name = columnObj;

    if(userCode !== '') {
        getiView.params["pusername"] = userCode;
    }
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    const resp = await axios
        .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue));

    logger.info(`Resp ::: ${resp.status} ==> ${JSON.stringify(resp.data)}`);

    return resp;
}
async function updateFeedback(columnObj, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = columnObj;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'feedb',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = changeTransData(savedata);

    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Signup paramValue ::: ${JSON.stringify(paramValue)}`);

    const resp = await axios
        .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue));

    logger.info(`Resp ::: ${resp.status} ==> ${JSON.stringify(resp.data)}`);

    return resp;
}

function getFilesizeInBytes(filename) {
    var stats = fs.statSync(filename)
    var fileSizeInBytes = stats["size"]
    return fileSizeInBytes
}

function doMd5(password) {

    return md5('1983'.concat(md5(password)));
}

async function getFiles(orderId, sessionId) {
    let getiView = {
        'name': 'gordfil',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'pcaseno': orderId
        }
    };

    getiView = changeTransData(getiView);

    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`getFiles paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

async function postFiles(fileObj, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = fileObj;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'ordfl',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order Files paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Files Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}

function changeTransData(getiView) {

    if (appType === 'b2c') {
        getiView.s = '';
        getiView["username"] = process.env.USERNAME;
        getiView["password"] = doMd5(process.env.PASSWORD);
    }

    return getiView;
}
function zeroFill(digit) {
    return (digit<10)?`0${digit}`:digit;
}

module.exports = CommonService = {
    updateFeedback,
    loadOrderData,
    getFilesizeInBytes,
    doMd5,
    postFiles,
    getFiles,
    changeTransData,
    zeroFill
};
