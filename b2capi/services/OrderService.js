const logger = require('../utilities/logger');
const axios = require('axios');
const CircularJSON = require('circular-json');
const commonService = require('./commonServices');
const serviceName = [
    'Scan Design',
    'Smile Design',
    'Smile Scan'
];

async function createB2CRxOrder(rxFormColumns, rxRowColumns) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = rxFormColumns;
    axp_recid1.push(axpRec);

    const axp_recid2 = [];
    const axpRec2 = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec2.columns = rxRowColumns;
    axp_recid2.push(axpRec2);

    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recData['axp_recid2'] = axp_recid2;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S,
        'transid': 'crxor',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = commonService.changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}

async function updateDsOrder(axpRecColumn, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = axpRecColumn;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'dsupd',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = commonService.changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        // http://192.168.12.8/mylabconnectx7scripts/ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}

async function createDsOrder(axpRec1Columns, recId, sessionId) {

    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = axpRec1Columns;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'dsord',
        'xmltext': '',
        'recordid': recId,
        'recdata': []
    }
    savedata = commonService.changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}

async function getDsOrderWithStatus(orderName) {
    let getiView = {
        'name': orderName,
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S,
        'sqlpagination': 'false',
        'params': {}
    };
    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    //logger.info(`DSOrder Result Data::: ${JSON.stringify(CircularJSON.stringify(result.data))}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}

async function processDsOrder(resultData) {

    const recordsData = JSON.parse(resultData);
    if (recordsData.result[0].error) {
        logger.error(`Result Error`);
    } else {
        const rows = recordsData.result[0].row;
        rows.forEach(
            rowData => {

                const date = new Date();
                const rxFormColumns = {
                    "transid": "rxore",
                    "recid": "0",
                    "username": rowData.dusername,
                    "servicetype": "B2C",
                    "labtechnician": "ramesh",
                    "patient": "Fganesh",
                    "doctor": "raja",
                    "doctorid": "RJ0393",
                    "labname": "Knight Dental Group (Oldsmar) Lab",
                    "duedate": `${date.getMonth()}/${date.getDate()}/${date.getFullYear()}`,
                    "toothshadesystem": "VITA Lumin",
                    "labid": "la556",
                    "servicename": serviceName[parseInt(rowData.servicetype)],
                    "status": "",
                    "nphoto": "",
                    "wphoto": ""
                }

                if (rowData.servicetype === '1' || rowData.servicetype === 1) {
                    rxFormColumns.wphoto = rowData.widesmailfile;
                    rxFormColumns.nphoto = rowData.normalsmilefile;
                }
                const rxRowColumns = {
                    "filename": "",
                    "filesize": "",
                    "material": "",
                    "designtype": "",
                    "tat": "",
                    "msg": "",
                    "nounit": "1"
                }
                if (rowData.servicetype !== '1' || rowData.servicetype !== 1) {
                    rxRowColumns.filename = rowData.fname;
                    rxRowColumns.material = rowData.material;
                    rxRowColumns.designtype = rowData.designtype;
                }

                createB2CRxOrder(rxFormColumns, rxRowColumns)
                    .then(
                        resultData => {
                            const result = JSON.parse(resultData);
                            if (result.result[0].error) {
                                logger.error(`Error Inside ::: ${JSON.stringify(result.result[0].error)}`)
                            } else {
                                const orderData = result.result[0].message[0];
                                const orderId = orderData["Rx Order No."];
                                logger.info(`RowData to Update::: ${JSON.stringify(rowData)}`);
                                const requestDetails = {
                                    "transid": "dsupd",
                                    "dsorderid": rowData.recordid,
                                    "orderno": orderId,
                                    "upload_status": "1",
                                    "archive_status": "0"
                                };

                                updateDsOrder(requestDetails)
                                    .then(
                                        resultData => {
                                            const resultValue = JSON.parse(resultData);
                                            if (resultValue.result[0].error) {
                                                logger.error(`Update DS Order Failed inside Job`);
                                            } else {
                                                logger.info(`Ds Order Updated Inside ${resultData}`);
                                            }
                                        }
                                    )
                                    .catch(
                                        reason => {
                                            logger.error(`Failed to update DS Order in Job`);
                                        }
                                    );
                            }
                        }
                    )
                    .catch(
                        reason => {
                            logger.error(`B2C Save Log Error ${reason} `);
                        }
                    );
            }
        );
    }
}

async function updateSmileOrder(columnObj) {

    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = columnObj;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S,
        'transid': 'upsmi',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = commonService.changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Signup paramValue ::: ${JSON.stringify(paramValue)}`);

    const resp = await axios
        .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue));

    logger.info(`Resp ::: ${resp.status} ==> ${JSON.stringify(resp.data)}`);

    return resp;
}

async function getOrderDetails(orderId, sessionId) {
    let getiView = {
        'name': 'dsfinalu',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'dsorderid': orderId
        }
    };
    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
              reason => {
                  return reject(reason);
              }
            );
    });
}

async function getOrderInfoDetails(orderId, sessionId) {
    let getiView = {
        'name': 'gordinf',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'pcaseno': orderId
        }
    };

    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

async function getRxOrderDetails(orderId, sessionId) {
    let getiView = {
        'name': 'getrxd',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'casenumber': orderId
        }
    };

    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

module.exports = OrderService = {
    createDsOrder,
    createB2CRxOrder,
    updateDsOrder,
    getDsOrderWithStatus,
    getOrderDetails,
    processDsOrder,
    updateSmileOrder,
    getOrderInfoDetails,
    getRxOrderDetails
};
