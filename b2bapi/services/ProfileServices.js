const logger = require('../utilities/logger');
const axios = require('axios');
const CircularJSON = require('circular-json');
const commonService = require('./commonServices');
async function getProfile(userId, sessionId) {
    let getiView = {
        'name': 'custprof',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {
            'pcustcode': userId
        }
    };
    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Profile paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    logger.error(`getProfile Error: ${reason}`);
                    return reject(reason);
                }
            );
    });
}

async function updateProfile(profileObject, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = profileObject;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'ucstm',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }

    savedata = commonService.changeTransData(savedata);

    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                logger.error(`getProfile Error: ${reason}`);
                return reject(reason);
            }
        )
    });
}
async function getDesignPreferenceValues(sessionId) {
    let getiView = {
        'name': 'gdprfval',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'sqlpagination': 'false',
        'params': {}
    };

    getiView = commonService.changeTransData(getiView);
    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Design Profile paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

async function updateDesignPreferenceValues(designProfileObj, sessionId) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = designProfileObj;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    let savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S || sessionId,
        'transid': 'udprf',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata = commonService.changeTransData(savedata);
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Order paramValue ::: ${JSON.stringify(paramValue)}`);
    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                saveData => {
                    logger.info(`Saved Response::: ${saveData}`);
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(saveData.data))));
                }
            ).catch(
            reason => {
                return reject(reason);
            }
        )
    });
}
module.exports = ProfileService = {
   getProfile,
   updateProfile,
   getDesignPreferenceValues,
   updateDesignPreferenceValues
};
