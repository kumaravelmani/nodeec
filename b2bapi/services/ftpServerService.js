const SftpClient = require('ssh2-sftp-client');
const fs = require("fs");
const logger = require("../utilities/logger");
const os=require("os");
const path=require("path");
const config = {
    host: process.env.FTP_SERVER_IP,
    port: 22,
    username: process.env.FTP_USERNAME,
    privateKey: fs.readFileSync(`${__dirname}/privatekey/sftpprivatekey.ppk`),
    retries: 3,
    retry_factor: 3,
    retry_minTimeout: 2000
};

const downloadOptions = {
    concurrency: 64, // integer. Number of concurrent reads to use
    chunkSize: 1048576, // integer. Size of each read in bytes
    step: function(total_transferred, chunk, total) {
        logger.info(`Total Download :: ${total_transferred}, chunk::: ${chunk}, ${total}`);
    }
};

const uploadOptions = {
    concurrency: 64, // integer. Number of concurrent reads
    chunkSize: 1048576, // integer. Size of each read in bytes
    mode: 0o755, // mixed. Integer or string representing the file mode to set
    step: function(total_transferred, chunk, total)  {
        logger.info(`Total Transferred :: ${total_transferred}, chunk::: ${chunk}, ${total}`);
    }// function. Called every time
    // a part of a file was transferred
}

const fileCopyOptions = {
    flags: 'w',  // w - write and a - append
    encoding: null, // use null for binary files
    mode: 0o666, // mode to use for created file (rwx)
    autoClose: true // automatically close the write stream when finished
};

async function uploadFile (remotePath, timeStamp, recordId, labId, fileName) {
    const client = new SftpClient("Upload_Files");
    return new Promise((resolve, reject) => {
        client.connect(config)
            .then(
                () => {
                    const directory = `${process.env.FTP_UPLOAD_DIR}${labId}`;
                    logger.info(`Directory for file Upload is ::: ${directory}`);
                    if (!client.exists(directory)) {
                        client.mkdir(directory, true)
                            .then(
                                data => {
                                    logger.info(`Information ${data}`);
                                }
                            )
                            .catch(
                                reason => {
                                    logger.error(`Reason ::: ${reason}`);
                                }
                            );
                    }
                    const newDirectory = `${directory}/${timeStamp}/${recordId}`;

                    client.mkdir(newDirectory, true)
                        .then(
                            data => {
                                logger.info(`newDirectory ${data}`);
                                const fileData = fs.createReadStream(remotePath);
                                const newFileToCopy = `${newDirectory}/${fileName}`;
                                client.fastPut(remotePath, newFileToCopy, uploadOptions)
                                    .then(
                                        fileData => {
                                            logger.info(`File Copied ::: ${fileData}`);
                                            fs.unlink(`${remotePath}`,(err) => {
                                                if (err) {
                                                    logger.error(`Unlink Error :: ${err}`);
                                                }

                                                logger.info(`File Deleted Successfully:: ${remotePath}`);
                                            });
                                        }
                                    )
                                    .catch(
                                        reason => {
                                            logger.error(`File Copy error::: ${reason}`);
                                        }
                                    )
                            }
                        )
                        .catch(
                            reason => {
                                logger.error(`New Reason ::: ${reason}`);
                            }
                        );
                    return resolve(client.cwd());
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

async function downloadFile (fileName, remotePath) {
    const client = new SftpClient("Download_Files");
    const localPath =path.join(`${os.tmpdir()}`,`${fileName}`);

    return new Promise((resolve, reject) => {
        client.connect(config)
            .then(
                () => {
                    const remoteFilePath = `${process.env.FTP_UPLOAD_DIR}/${remotePath}/${fileName}`;
                    logger.info(`file to  download is ::: ${remoteFilePath}`);
                    client.fastGet(remoteFilePath,localPath, downloadOptions )
                        .then(
                            fileData => {
                                logger.info(`File Downloaded from FTP:: ${fileData}`);
                                return resolve(localPath);
                            }
                        )
                        .catch(
                            reason => {
                                logger.error(`Download Failed ::: ${reason}`);
                                return reject(reason);
                            }
                        );
                }
            )
            .catch(
                reason => {
                    logger.error(`Connect failed ${reason}`);
                    return reject(reason);
                }
            );
    });
}

module.exports = ftpServerService = {
    uploadFile,
    downloadFile
};
