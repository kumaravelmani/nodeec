const md5 = require('md5');
const axios = require('axios');
const logger = require('../utilities/logger');

async function doAuthenticate(userName, passWord) {
    logger.info(`Hash:: ${md5(passWord)}`);
    let hashPassword = md5('1983'.concat(md5(passWord)));

    const loginModel = {
        'axpapp': 'easyconnect',
        'username': 'chatbot',
        'password': '59390203efefa4b9e11bc0631a53bca2',
        'seed': '1983',
        'other': 'Chrome'
    };
    loginModel.username = userName;
    loginModel.password = hashPassword;
    const login = {};
    const paramValue = {};
    const _parameter = [];

    const sessionId = '';

    login['login'] = loginModel;
    _parameter.push(login);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    const resp = await axios
        .post(`${process.env.API_URL}ASBMenuRest.dll/datasnap/rest/TASBMenuREST/Login`, JSON.stringify(paramValue));

    logger.info(`Resp ::: ${resp.status} ==> ${JSON.stringify(resp.data)}`);

    return resp;
}


async function doSignUp(signUpColumns) {

    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = signUpColumns;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    const savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',	
	'username': 'admin',
	        
	'password': 'a5ca360e803b868680e2b6f7805fcb9e',
        'transid': 'custm',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Signup paramValue ::: ${JSON.stringify(paramValue)}`);

    const resp = await axios
        .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue));

    logger.info(`Resp ::: ${resp.status} ==> ${JSON.stringify(resp.data)}`);

    return resp;
}

async function validateUsername(userName) {
    const getiView = {
        'name': 'buserval',
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S,
        'sqlpagination': 'false',
        'params': {
            'pusername': userName
        }
    };

    const getiview = {};
    const paramValue = {};
    const _parameter = [];

    getiview['getiview'] = getiView;
    _parameter.push(getiview);
    paramValue['_parameters'] = _parameter;
    logger.info(`Final paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBIViewRest.dll/datasnap/rest/TASBIViewREST/getiview`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            );
    });
}

async function resetPassword(passwordColumnObj) {
    const axp_recid1 = [];
    const axpRec = {
        'rowno': '001',
        'text': '0',
        'columns': {}
    };
    axpRec.columns = passwordColumnObj;
    axp_recid1.push(axpRec);
    const recdata = [];
    const recData = {};
    recData['axp_recid1'] = axp_recid1;
    recdata.push(recData);
    const savedata = {
        'axpapp': 'easyconnect',
        'seed': '1983',
        's': process.env.S,
        'transid': 'reset',
        'xmltext': '',
        'recordid': '0',
        'recdata': []
    }
    savedata.recdata = recdata;
    const saveData = {};
    saveData['savedata'] = savedata;
    const _parameters = [];
    _parameters.push(saveData);

    const paramValue = {};
    paramValue['_parameters'] = _parameters;

    logger.info(`Signup paramValue ::: ${JSON.stringify(paramValue)}`);

    return new Promise((resolve, reject) => {
        axios
            .post(`${process.env.API_URL}ASBTStructRest.dll/datasnap/rest/TASBTStruct/savedata`, JSON.stringify(paramValue))
            .then(
                result => {
                    return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.data))));
                }
            )
            .catch(
                reason => {
                    return reject(reason);
                }
            )
    });

}
module.exports = LoginService = {
    doAuthenticate,
    doSignUp,
    resetPassword,
    validateUsername
};


