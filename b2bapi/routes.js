const express = require("express");
const lgModule = require("./controllers/login.controller");
const signupModule = require('./controllers/signup.controller');
const feedbackModule = require('./controllers/feedback.controller');
const orderDataModule = require('./controllers/orderdata.controller');
const profileModule = require('./controllers/profile.controller');
const userAccountModule = require('./controllers/userAccount.controller');
const dashBoardModule = require('./controllers/dashboard.controller');
const messageModule = require('./controllers/message.controller');
const s3Module = require('./controllers/s3.controller');
const routes = express.Router();

routes.use("/login", lgModule.login);
routes.use("/signup", signupModule.doSignup);

routes.use("/updateFeedback", feedbackModule.updateFeedback);

routes.use("/loadOrderData", orderDataModule.getOrderData);
routes.use("/updateSmileOrderData", orderDataModule.updateSmileOrderData);
routes.use("/getOrderDetails", orderDataModule.getOrderDetails);
routes.use("/getSearchOrderDetails", orderDataModule.getSearchOrderDetails);

routes.use("/sendResetLink", signupModule.sendResetLink);
routes.use("/resetPassword", signupModule.resetPassword);

routes.use("/getProfileData", profileModule.getProfileData);
routes.use("/updateProfile", profileModule.updateProfile);
routes.use("/getDesignPreferenceValues", profileModule.getDesignPreferenceValues);
routes.use("/updateDesignPreferenceValues", profileModule.updateDesignPreferenceValues);

routes.use("/getUserAccounts", userAccountModule.getUserAccounts);
routes.use("/addNewUserAccount", userAccountModule.addNewUserAccount);
routes.use("/updateUserAccount", userAccountModule.updateUserAccount);

routes.use("/getDashboardSnapshot", dashBoardModule.getDashboardSnapshot);
routes.use("/getDashBoardTatTrend", dashBoardModule.getDashBoardTatTrend);

routes.use("/getRxOrderDetails", orderDataModule.getRxOrderDetails);

routes.use("/sendMessage", messageModule.sendMessage);
routes.use("/getOrderMessage", messageModule.getOrderMessage);
routes.use("/postFiles", messageModule.postFiles);
routes.use("/getFiles", messageModule.getFiles);

routes.use("/getSignedUrl", s3Module.getSignedUrl);
routes.use("/getDownloadUrl", s3Module.getFullSignedUrl);

routes.use("/downloadMultifile", s3Module.downloadAllFiles);

module.exports = routes;
