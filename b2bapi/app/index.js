const express = require("express");
const cors = require("cors");
const https = require('https');
const http = require('http');
const routes = require("../routes");
const bodyParser = require("body-parser");
const logger = require("../utilities/logger");
const orderService = require('../services/OrderService');
const ftpServerService = require('../services/ftpServerService');
const commonService = require('../services/commonServices');
const mime = require("node-mime");
// const dsOrderService = require('../db/dsOrder.services');
const wasabiService = require('../services/WasabiServices');
const cron = require("node-cron");
const fileUpload = require('express-fileupload');

const fs = require('fs');
const os = require('os');
const path = require('path');
let privateKey = fs.readFileSync(`${__dirname}/certificates/server.key`, 'utf8');
let certificate = fs.readFileSync(`${__dirname}/certificates/server.crt`, 'utf8');
if (process.env.APP_TYPE === 'b2b') {
    logger.info(`Loaded B2B Certificates ${process.env.APP_TYPE}`);
    privateKey = fs.readFileSync(`${__dirname}/certificates/b2b/server.key`, 'utf8');
    certificate = fs.readFileSync(`${__dirname}/certificates/b2b/server.crt`, 'utf8');
}

const credentials = {key: privateKey, cert: certificate};

const app = express();
app.use(cors());
// Mount API routes
app.use((req, res, next) => {
    res.header("Access-Control-Allow-Origin", "*");
    res.header(
        "Access-Control-Allow-Headers",
        "Origin, X-Requested-With, Content-Type, Accept"
    );
    next();
});

app.use(fileUpload());

app.use(bodyParser.json({limit: '50mb'}));
app.use(bodyParser.urlencoded({extended: true, limit: '50mb'}));

app.use("/", routes);

const serviceName = [
    'Scan Design',
    'Smile Design',
    'Smile Scan'
];

cron.schedule("* * * * *", () => {
    //logger.info(`STarted:::`);
    /*
        orderService.getDsOrderWithStatus('upldzero')
            .then(
                resultData => {
                    //logger.info(`resultData::: ${resultData}`);
                    orderService.processDsOrder(resultData);
                }
            ).catch(
            reason => {
                logger.error(`errorData ::: ${reason}`);
            }
        );

         */

});


app.use(function (error, req, res, next) {
    logger.info(`Response::: ${res}`);
    res.status(error.status || 500);
    logger.info(`Error::: ${JSON.stringify(error)}`);

    res.json({
        error: {
            message: error.message
        }
    });
});

app.get('/downloadFile', function (req, res) {

    const orderId = req.query.orderId;
    const sessionId = req.headers.sessionid;

    orderService.getOrderInfoDetails(orderId, sessionId)
        .then(
            result => {
                const resultData = JSON.parse(result);
                if (resultData.result[0].error) {
                    res.status(500).send(resultData.result[0].error);
                } else {
                    const recordData = resultData.result[0].row[0];
                    if (recordData.filein === '0' || recordData.filein === '') {
                        ftpServerService.downloadFile(recordData.fname, recordData.fpath)
                            .then(
                                response => {
                                    logger.info(`Local FIle Path ::: ${response}`);
                                    const downloadFilePath = path.join(os.tmpdir(), recordData.fname);
                                    logger.info(`Download File Path ::: ${downloadFilePath}`);
                                    const fileName = recordData.fname;
                                    const mimeType = mime.lookUpType(downloadFilePath);
                                    logger.info(`MimeType is ${mimeType}`);
                                    res.setHeader('Content-disposition', 'attachment; filename=' + fileName);
                                    res.setHeader('Content-type', mimeType);
                                    const fileStream = fs.createReadStream(downloadFilePath);
                                    fileStream.pipe(res);
                                }
                            )
                            .catch(
                                reason => {
                                    logger.info(`Error in Main Block::: ${reason}`);
                                    res.status(500).send(reason);
                                }
                            );
                    } else {
                        res.status(500).send("No files to download.");
                    }
                }
            }
        )
        .catch(
            reason => {
                logger.error(`Main Block error ${reason}`);
                res.status(500).send(reason);
            }
        );
});

app.post('/attachMessageFile', function (req, res) {

    const responseMessage = {
        status: false,
        message: '',
        data: '',
        location: '',
        fileName: ''
    };
    try {
        if (!req.files || Object.keys(req.files).length === 0) {
            return res.status(400).send('No files were uploaded.');
        }
        logger.info(`Coming inside`);
        const fileToUpload = req.files.filetoattach;
        const orderId = req.headers.orderid;
        const sourceType = req.headers.sourcetype;
        const dsOrderId = req.headers.dsorderid;
        const sessionId = req.headers.sessionid;
        logger.info(`Order ID: ${orderId}`);
        const curDate = new Date();
        const curTimeStamp = `${curDate.getFullYear()}${curDate.getMonth() + 1}${curDate.getDate()}${curDate.getHours()}${curDate.getMinutes()}${curDate.getSeconds()}`;

        const toLocation = `${process.env.DIR_PATH}\\${orderId}\\${curTimeStamp}\\${sourceType}\\`;

        if (!fs.existsSync(toLocation)) {
            logger.info(`Creating toLocation directory:: ${toLocation}`);
            fs.mkdir(toLocation, {recursive: true}, (err) => {
                if (err) throw err;

                fileToUpload.mv(`${toLocation}${fileToUpload.name}`, function (err) {
                    if (err) {
                        responseMessage.status = false;
                        responseMessage.message = `Failed to Upload the file. Err: ${err}`;
                        res.status(500).send(responseMessage);
                    }
                    orderService.getOrderInfoDetails(orderId, sessionId)
                        .then(
                            result => {
                                const resultData = JSON.parse(result);
                                if (resultData.result[0].error) {
                                    responseMessage.status = false;
                                    responseMessage.message = resultData.result[0].error;
                                    res.status(500).send(responseMessage);
                                }
                                const recordData = resultData.result[0].row[0];
                                let fileLocation = recordData.fpath;
                                if (recordData.fpath.indexOf('/Initial') !== -1) {
                                    const arrData = recordData.fpath.split('/Initial');
                                    fileLocation = arrData[0];
                                }
                                const wasabiDirectory = `${fileLocation}/${sourceType}/${fileToUpload.name}`;
                                logger.info(`Wasabi Directory: ${wasabiDirectory}`);
                                wasabiService.uploadFile(`${toLocation}${fileToUpload.name}`, wasabiDirectory)
                                    .then(
                                        result => {
                                            responseMessage.status = true;
                                            responseMessage.message = "File uploaded successfully."
                                            responseMessage.data = result;
                                            responseMessage.location = `${fileLocation}/${sourceType}`;
                                            responseMessage.fileName = fileToUpload.name;
                                            let sourceValue = sourceType;
                                            if (sourceType.indexOf('_') !== -1) {
                                                sourceValue = sourceType.split('_').join(' ');
                                            }
                                            const fileObj = {
                                                "ds_ordersid": dsOrderId,
                                                "caseno": orderId,
                                                "filetype": sourceValue,
                                                "filename": `${responseMessage.location}/${responseMessage.fileName}`
                                            }
                                            commonService.postFiles(fileObj, sessionId)
                                                .then(
                                                    result => {
                                                        logger.info(`Post files updated successfully::: ${result}`);
                                                        res.status(200).send(responseMessage);
                                                    }
                                                )
                                                .catch(
                                                    reason => {
                                                        logger.error(`Failed to update the postfiles ${reason}`);
                                                        responseMessage.message = `${responseMessage.message} Postfile failed ${reason}`;
                                                        res.status(200).send(responseMessage);
                                                    }
                                                );
                                        }
                                    )
                                    .catch(
                                        reason => {
                                            responseMessage.status = false;
                                            responseMessage.message = `Failed to Upload the file. Reason: ${reason}`;
                                            res.status(500).send(responseMessage);
                                        }
                                    );
                            }
                        )
                        .catch(
                            reason => {
                                responseMessage.status = false;
                                responseMessage.message = `Failed to Get and Upload the file. Reason: ${reason}`;
                                res.status(500).send(responseMessage);
                            }
                        );
                });
            });
        }

        logger.info(`File To Upload: ${toLocation}`);

    } catch (err) {
        logger.error(`Error ${err}`);
        responseMessage.status = false;
        responseMessage.message = `Failed to Upload the file. Error: ${err}`;
        res.status(500).send(responseMessage);
    }

});

app.post('/finalSmileUpload', function (req, res) {
    logger.info(`Final Smile Upload Initiated`);
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    try {

        const finalSmile = req.files.finalsmile;
        const orderId = req.headers.dsorderid;

        const curDate = new Date();
        const curTimeStamp = `${curDate.getFullYear()}${curDate.getMonth() + 1}${curDate.getDate()}`;
        let fileFExt = finalSmile.name;
        fileFExt = fileFExt.substr(fileFExt.lastIndexOf('.') + 1, fileFExt.length);

        const finalSMile = `${orderId}_${curTimeStamp}_f.${fileFExt}`;

        const directoryTo = `${process.env.DIR_PATH}\\${orderId}\\smile\\${finalSMile}`;
        const virtualPath = `${process.env.DIR_VIR_PATH}/${orderId}/smile/`;

        logger.info(`Directory::: ${directoryTo} ::: Virtual ::: ${virtualPath}`);

        finalSmile.mv(directoryTo, function (err) {
                if (err)
                    return res.status(500).send(err);
                orderService.getOrderDetails(orderId)
                    .then(
                        result => {
                            const resultData = JSON.parse(result);
                            if (resultData.result[0].error) {
                                logger.error(`Result Data Error:: ${JSON.stringify(resultData.result[0].error)}`);
                                return res.status(500).send(resultData.result[0].error);
                            } else {
                                logger.info(`Result Data ::: ${resultData}`);
                                const recordData = resultData.result[0].row[0];
                                const updateReq = {
                                    'transid': 'dsupd',
                                    'dsorderid': orderId,
                                    'orderno': recordData.orderno,
                                    'upload_status': '1',
                                    'archive_status': '1',
                                    'fname': recordData.filename,
                                    'normalsmilefile': recordData.normalsmilefile,
                                    'widesmailfile': recordData.widesmailfile,
                                    'finalsmilefile': `${virtualPath}/${finalSMile}`,
                                    'status': '2'
                                }
                                orderService.updateDsOrder(updateReq, '232842.850460.365081')
                                    .then(
                                        result => {
                                            const resultData = JSON.parse(result);
                                            if (resultData.result[0].error) {
                                                logger.error(`Failed Final Smile to update the DS Order ${resultData.result[0].error}`);
                                            } else {
                                                return res.status(200).send(
                                                    `{"Status":"Success", "Message":"File uploaded successfully and ${resultData.result[0].message[0]}"`
                                                );
                                            }
                                        }
                                    )
                                    .catch(
                                        reason => {
                                            logger.error(`smile FinalUnable to update the Ds Ordrr ${reason}`);
                                            return res.status(500).send(reason);
                                        }
                                    );
                            }
                        }
                    )
                    .catch(
                        reason => {
                            logger.error(`Reason Error::: ${reason}`);
                            return res.status(500).send(reason);
                        }
                    );
            }
        );

    } catch (e) {
        logger.error(`Error::: ${e}`);
        return res.status(500).send(e);
    }
});

app.post("/easyUpload", function (req, res) {
    if (!req.files || Object.keys(req.files).length === 0) {
        return res.status(400).send('No files were uploaded.');
    }
    try {
        const scanFile = req.files.scanfile;

        const wideSmile = req.files.widesmile;

        const normalSmile = req.files.normalsmile;

        const taTimes = req.headers.tat;

        const materialType = req.headers.material;

        const designType = req.headers.design;

        const messages = req.headers.msg;

        const serviceType = req.headers.servicetype || 0;

        const userCode = req.headers.usercode;

        const firstName = req.headers.firstname;

        const lastName = req.headers.lastname;

        const age = req.headers.age;

        const labId = req.headers.lab || userCode;

        const gender = req.headers.gender;

        const sessionId = req.headers.sessionid;

        logger.info(`Upload Inn ::: ${JSON.stringify(serviceType)}, ${wideSmile}, ${normalSmile}`);

        let dir = process.env.DIR_PATH;

        if (!fs.existsSync(dir)) {
            logger.info(`Creating new directory:: ${dir}`);
            fs.mkdir(dir, {recursive: true}, (err) => {
                if (err) throw err;
            });
        }
        logger.info(`Directoy ::: ${dir}`);

        const requestDetails = {
            'transid': ' dsord',
            'recid': '',
            'fname': '',
            'orderno': '',
            'normalsmilefile': '',
            'widesmailfile': '',
            'upload_status': '0',
            'archive_status': '0',
            'material': materialType,
            'designtype': designType,
            'tat': taTimes,
            'msg': messages,
            'labcode': labId,
            'createdat': new Date(),
            'updatedat': new Date(),
            'servicetype': serviceType,
            'status': '0',
            'dusername': userCode,
            'normal_smile_url': '',
            'wide_smile_url': '',
            'final_smile_url': '',
            'final_scan_url': '',
            'qc_rating': '',
            'qc_remarks': '',
            'p_firstname': firstName,
            'p_lastname': lastName,
            'p_age': age,
            'p_gender': gender
        };

        try {
            orderService.createDsOrder(requestDetails, '0', sessionId).then(
                result => {
                    logger.info(`B2CResult ::: ${result}`);
                    const resultData = JSON.parse(result);
                    const createResponseData = resultData;
                    if (resultData.result[0].error) {
                        logger.error(`B2CBlock:: Unable to create B2C Ds-Order ${resultData.result[0].error}`);
                        return res.status(500).send(resultData.result[0].error);
                    } else {
                        const messageObj = resultData.result[0].message[0];
                        const recordId = messageObj.recordid;
                        const caseNo = messageObj.caseno;
                        const curDate = new Date();

                        if (serviceType === 0 || serviceType === '0' || serviceType === '3' || serviceType === 3) {

                            const directoryTo = `${dir}${recordId}\\scan\\`;
                            const virtualPath = `${process.env.DIR_VIR_PATH}/${recordId}/scan/`;

                            if (!fs.existsSync(directoryTo)) {
                                fs.mkdir(directoryTo, {recursive: true}, (err) => {
                                    if (err) throw err;

                                    logger.info(`New Directory Created ${directoryTo}`);
                                    const curTimeStamp = `${curDate.getFullYear()}${commonService.zeroFill(curDate.getMonth() + 1)}${commonService.zeroFill(curDate.getDate())}`;
                                    let fileSExt = scanFile.name;
                                    fileSExt = fileSExt.substr(fileSExt.lastIndexOf('.') + 1, fileSExt.length);
                                    let fileType = "Initial_Scan";
                                    if (fileSExt.toLowerCase() === 'stl') {
                                        fileType = 'Initial_STL';
                                    }
                                    const scannedFile = `${recordId}_${curTimeStamp}_s.${fileSExt}`;

                                    scanFile.mv(`${directoryTo}\\${scanFile.name}`, function (err) {
                                        if (err) throw err;

                                        const wasabiDirectory = `${labId}/${curTimeStamp}/${caseNo}/${fileType}/${scanFile.name}`;
                                        wasabiService.uploadFile(`${directoryTo}\\${scanFile.name}`, wasabiDirectory)
                                            .then(
                                                data => {
                                                    const updateReq = {
                                                        'transid': 'dsupd',
                                                        'dsorderid': recordId,
                                                        'orderno': '',
                                                        'archive_status': '1',
                                                        'fpath': `${labId}/${curTimeStamp}/${caseNo}/${fileType}`,
                                                        'fname': `${scanFile.name}`,
                                                        'filein': '1',
                                                        'filesize': commonService.getFilesizeInBytes(`${directoryTo}\\${scanFile.name}`),
                                                        'finalsmilefile': '',
                                                        'normalsmilefile': '',
                                                        'widesmailfile': '',
                                                    }
                                                    const fileObj = {
                                                        "ds_ordersid": recordId,
                                                        "caseno": caseNo,
                                                        "filetype": fileType.split('_').join(' '),
                                                        "filename": `${labId}/${curTimeStamp}/${caseNo}/${fileType}/${scanFile.name}`
                                                    }
                                                    commonService.postFiles(fileObj, sessionId)
                                                        .then(
                                                            result => {
                                                                logger.info(`Post files updated successfully::: ${result}`);
                                                                orderService.updateDsOrder(updateReq, sessionId)
                                                                    .then(
                                                                        result => {
                                                                            const resultData = JSON.parse(result);
                                                                            if (resultData.result[0].error) {
                                                                                logger.error(`SC: Failed to update the DS Order ${JSON.stringify(resultData.result[0].error)}`);
                                                                                return res.status(500).send(resultData.result[0].error);
                                                                            } else {
                                                                                return res.status(200).send(
                                                                                    `{"Status":"Success", "Message":"Files uploaded successfully", "data": ${JSON.stringify(createResponseData.result[0].message[0])}}`
                                                                                );
                                                                            }
                                                                        }
                                                                    )
                                                                    .catch(
                                                                        reason => {
                                                                            logger.error(`SC: Unable to update the Ds Order ${reason}`);
                                                                            return res.status(500).send(reason);
                                                                        }
                                                                    );
                                                            }
                                                        )
                                                        .catch(
                                                            reason => {
                                                                logger.error(`Failed to update the postfiles ${reason}`);
                                                                return res.status(200).send(
                                                                    `{"Status":"Success", "Message":"Files uploaded successfully", "data": ${JSON.stringify(createResponseData.result[0].message[0])}}`
                                                                );
                                                            }
                                                        );
                                                }
                                            )
                                            .catch(
                                                reason => {
                                                    logger.error(`Failed to Move to S3 Server ${reason}`);
                                                }
                                            );
                                    });
                                });
                            }
                        } else {
                            const directoryTo = `${dir}${recordId}\\smile\\`;
                            const virtualPath = `${process.env.DIR_VIR_PATH}/${recordId}/smile/`;

                            if (!fs.existsSync(directoryTo)) {
                                fs.mkdir(directoryTo, {recursive: true}, (err) => {
                                    if (err) throw err;
                                    logger.info(`New Directory Created ${directoryTo}`);
                                    const curTimeStamp = `${curDate.getFullYear()}${commonService.zeroFill(curDate.getMonth() + 1)}${commonService.zeroFill(curDate.getDate())}`;
                                    let fileWExt = wideSmile.name;
                                    fileWExt = fileWExt.substr(fileWExt.lastIndexOf('.') + 1, fileWExt.length);
                                    let fileNExt = normalSmile.name;
                                    fileNExt = fileNExt.substr(fileNExt.lastIndexOf('.') + 1, fileNExt.length);

                                    const wideSMile = `${recordId}_${curTimeStamp}_w.${fileWExt}`;
                                    const normalSMile = `${recordId}_${curTimeStamp}_n.${fileNExt}`;
                                    wideSmile.mv(`${directoryTo}\\${wideSMile}`, function (err) {
                                        if (err)
                                            return res.status(500).send(err);

                                        normalSmile.mv(`${directoryTo}\\${normalSMile}`, function (err) {
                                            if (err)
                                                return res.status(500).send(err);

                                            const wasabiWideDirectory = `${labId}/${curTimeStamp}/${caseNo}/Initial_Smile/${wideSmile.name}`;
                                            const wasabiNormalDirectory = `${labId}/${curTimeStamp}/${caseNo}/Initial_Smile/${normalSmile.name}`;
                                            wasabiService.uploadFile(`${directoryTo}\\${wideSMile}`, wasabiWideDirectory)
                                                .then(
                                                    data => {
                                                        wasabiService.uploadFile(`${directoryTo}\\${normalSMile}`, wasabiNormalDirectory)
                                                            .then(
                                                                data => {
                                                                    const fileObj = {
                                                                        "ds_ordersid": recordId,
                                                                        "caseno": caseNo,
                                                                        "filetype": 'Initial Wide Smile',
                                                                        "filename": `${wasabiWideDirectory}`
                                                                    }
                                                                    commonService.postFiles(fileObj, sessionId)
                                                                        .then(
                                                                            result => {
                                                                                logger.info(`Post files updated successfully::: ${result}`);
                                                                                const fileObj = {
                                                                                    "ds_ordersid": recordId,
                                                                                    "caseno": caseNo,
                                                                                    "filetype": 'Initial Normal Smile',
                                                                                    "filename": `${wasabiNormalDirectory}`
                                                                                }
                                                                                commonService.postFiles(fileObj, sessionId)
                                                                                    .then(
                                                                                        resultData => {
                                                                                            logger.info(`Post files updated successfully::: ${resultData}`);
                                                                                            const updateReq = {
                                                                                                'transid': 'dsupd',
                                                                                                'dsorderid': recordId,
                                                                                                'orderno': '',
                                                                                                'upload_status': '1',
                                                                                                'archive_status': '1',
                                                                                                'fname': '',
                                                                                                'normalsmilefile': `${wasabiNormalDirectory}`,
                                                                                                'widesmailfile': `${wasabiWideDirectory}`,
                                                                                                'finalsmilefile': ''
                                                                                            }
                                                                                            orderService.updateDsOrder(updateReq, sessionId)
                                                                                                .then(
                                                                                                    result => {
                                                                                                        const resultData = JSON.parse(result);
                                                                                                        if (resultData.result[0].error) {
                                                                                                            logger.error(`Faile to update the DS Order ${JSON.stringify(resultData.result[0].error)}`);
                                                                                                            if (serviceType === '1' || serviceType === 1) {
                                                                                                                res.status(500).send(resultData.result[0].error);
                                                                                                            }
                                                                                                        } else {
                                                                                                            if (serviceType === '1' || serviceType === 1) {
                                                                                                                return res.status(200).send(
                                                                                                                    `{"Status":"Success", "Message":"Files uploaded successfully", "data": ${JSON.stringify(createResponseData.result[0].message[0])}}`
                                                                                                                );
                                                                                                            } else {
                                                                                                                logger.info('Smile Updated in DS Order : ')
                                                                                                            }
                                                                                                        }
                                                                                                    }
                                                                                                )
                                                                                                .catch(
                                                                                                    reason => {
                                                                                                        logger.error(`Unable to update the Ds Ordrr ${reason}`);
                                                                                                        return res.status(500).send(reason);
                                                                                                    }
                                                                                                );
                                                                                        }
                                                                                    )
                                                                                    .catch(
                                                                                        reason => {
                                                                                            logger.error(`Post files Failed ::: ${reason}`);
                                                                                        }
                                                                                    );
                                                                            }
                                                                        )
                                                                        .catch(
                                                                            reason => {
                                                                                logger.error(`Post files Failed ::: ${reason}`);
                                                                            }
                                                                        );
                                                                }
                                                            )
                                                            .catch(
                                                                reason => {
                                                                    logger.error(`Failed here ${reason}`);
                                                                }
                                                            );
                                                    }
                                                )
                                                .catch(
                                                    reason => {

                                                    }
                                                );
                                        });
                                    });
                                });
                            }
                            if (serviceType === '2' || serviceType === 2) {
                                const directoryScanTo = `${dir}${recordId}\\scan\\`;
                                const virtualScanPath = `${process.env.DIR_VIR_PATH}/${recordId}/scan/`;

                                if (!fs.existsSync(directoryScanTo)) {
                                    fs.mkdir(directoryScanTo, {recursive: true}, (err) => {
                                        if (err) throw err;

                                        logger.info(`New Directory Created ${directoryScanTo}`);
                                        const curTimeStamp = `${curDate.getFullYear()}${curDate.getMonth() + 1}${curDate.getDate()}`;
                                        let fileSExt = scanFile.name;
                                        fileSExt = fileSExt.substr(fileSExt.lastIndexOf('.') + 1, fileSExt.length);
                                        const scannedFile = `${recordId}_${curTimeStamp}_s.${fileSExt}`;

                                        scanFile.mv(`${directoryScanTo}\\${scanFile.name}`, function (err) {
                                            if (err) throw err;

                                            const fileTotalSize = commonService.getFilesizeInBytes(`${directoryScanTo}\\${scanFile.name}`);

                                            const wasabiDirectory = `${labId}/${curTimeStamp}/${caseNo}/Initial_Scan/${scanFile.name}`;
                                            wasabiService.uploadFile(`${directoryScanTo}\\${scanFile.name}`, wasabiDirectory)
                                                .then(
                                                    data => {
                                                        const updateReq = {
                                                            'transid': 'dsupd',
                                                            'dsorderid': recordId,
                                                            'orderno': '',
                                                            'upload_status': '1',
                                                            'archive_status': '0',
                                                            'fpath': `${labId}/${curTimeStamp}/${caseNo}/Initial_Scan`,
                                                            'fname': `${scanFile.name}`,
                                                            'filesize': fileTotalSize
                                                        }

                                                        const fileObj = {
                                                            "ds_ordersid": recordId,
                                                            "caseno": caseNo,
                                                            "filetype": 'Initial Scan',
                                                            "filename": `${labId}/${curTimeStamp}/${caseNo}/Initial_Scan/${scanFile.name}`
                                                        }
                                                        commonService.postFiles(fileObj, sessionId)
                                                            .then(
                                                                result => {
                                                                    logger.info(`Post files updated successfully::: ${result}`);
                                                                    orderService.updateDsOrder(updateReq, sessionId)
                                                                        .then(
                                                                            result => {
                                                                                const resultData = JSON.parse(result);
                                                                                if (resultData.result[0].error) {
                                                                                    logger.error(`SC: Failed to update the DS Order ${resultData.result[0].error}`);
                                                                                } else {
                                                                                    return res.status(200).send(
                                                                                        `{"Status":"Success", "Message":"Files uploaded successfully", "data": ${JSON.stringify(createResponseData.result[0].message[0])}}`
                                                                                    );
                                                                                }
                                                                            }
                                                                        )
                                                                        .catch(
                                                                            reason => {
                                                                                logger.error(`SC: Unable to update the Ds Order ${reason}`);
                                                                                return res.status(500).send(reason);
                                                                            }
                                                                        );
                                                                }
                                                            )
                                                            .catch(
                                                                reason => {
                                                                    logger.error(`Failed to update the postfiles ${reason}`);
                                                                    return res.status(200).send(
                                                                        `{"Status":"Success", "Message":"Files uploaded successfully", "data": ${JSON.stringify(createResponseData.result[0].message[0])}}`
                                                                    );
                                                                }
                                                            );
                                                    }
                                                )
                                                .catch(
                                                    reason => {

                                                    }
                                                );
                                        });
                                    });
                                }
                            }
                        }
                    }
                }
            ).catch(
                reason => {
                    logger.error(`B2CInnerBlock:: Unable to create B2B Ds-Order ${reason}`);
                    return res.status(500).send(reason);
                }
            )
        } catch (e) {
            logger.error(`B2BOuterBlock:: Unable to create B2B Ds Order ${e}`);
            return res.status(500).send(e);
        }
    } catch (err) {
        logger.error(`Error ${err}`);
        return res.status(400).send('No files were uploaded.');
    }
});

// to overrite port via environmant variables
const port = process.env.PORT || 4201;

var httpsServer = https.createServer(credentials, app);
var httpServer = http.createServer(app);

if (process.env.NODE_ENV === 'production') {
    httpsServer.listen(port, function () {
        logger.info(`Port: ${port}`);
        logger.info(`Env: ${app.get("env")}`);
    });
} else {
    httpServer.listen(port, function () {
        logger.info(`Port: ${port}`);
        logger.info(`Env: ${app.get("env")}`);
    });
}


