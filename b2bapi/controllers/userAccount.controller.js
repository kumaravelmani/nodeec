const userAccountService = require('../services/UserAccountService');
const logger = require('../utilities/logger');

const userAccountModule = {
    getUserAccounts: async (req, res, next) => {
        try {
            const userId = req.query.userId;
            const sessionId = req.headers.sessionid;

            userAccountService.getUserAccounts(userId, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    addNewUserAccount: async (req, res, next) => {
        try {
            const userAccountObj = req.body;
            const sessionId = req.headers.sessionid;
            userAccountService.addNewUserAccount(userAccountObj, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateUserAccount: async (req, res, next) => {
        try {
            const userAccountObj = req.body;
            const sessionId = req.headers.sessionid;
            userAccountService.updateUserAccount(userAccountObj, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = userAccountModule;
