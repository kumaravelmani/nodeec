const logger = require('../utilities/logger');
const profileService = require('../services/ProfileServices');

const profileModule = {
    getProfileData: async (req, res, next) => {
        try {
            const userId = req.query.userId;
            const sessionId = req.headers.sessionid;

            profileService.getProfile(userId, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`getProfile Error1: ${reason}`);
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateProfile:  async (req, res, next) => {
        try {
            const profileObject = req.body;
            const sessionId = req.headers.sessionid;
            profileService.updateProfile(profileObject, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`getProfile Error1: ${reason}`);
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateDesignPreferenceValues:  async (req, res, next) => {
        try {
            const profileObject = req.body;
            const sessionId = req.headers.sessionid;
            profileService.updateDesignPreferenceValues(profileObject, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getDesignPreferenceValues: async (req, res, next) => {
        try {
            const sessionId = req.headers.sessionid;

            profileService.getDesignPreferenceValues(sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = profileModule;
