const dashBoardService = require('../services/DashBoardService');
const logger = require('../utilities/logger');

const dashBoardModule = {
    getDashboardSnapshot: async (req, res, next) => {
        try {
            const userId = req.query.userName;
            const sessionId = req.headers.sessionid;

            dashBoardService.getDashboardSnapshot(userId, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getDashBoardTatTrend: async (req, res, next) => {
        try {
            const userId = req.query.userName;
            const sessionId = req.headers.sessionid;

            dashBoardService.getDashBoardTatTrend(userId, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {

                            const allRows = resultData.result[0].row;
                            const newRows = [];
                            allRows.forEach(
                                value => {
                                    const rowValue = {
                                        "rowno": parseInt(value.rowno),
                                        "odate": value.odate,
                                        "mno": parseInt(value.mno),
                                        "completed": parseInt(value.completed),
                                        "intat": parseInt(value.intat)
                                    }
                                    newRows.push(rowValue);
                                }
                            );
                            res.status(200).send(newRows);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = dashBoardModule;
