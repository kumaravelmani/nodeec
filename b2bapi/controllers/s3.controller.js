const orderService = require('../services/OrderService');
const wasabiService = require('../services/WasabiServices');
const path = require('path');
const logger = require('../utilities/logger');
const s3Module = {
    getSignedUrl: async (req, res, next) => {
        try {
            const orderId = req.query.orderId;
            const sessionId = req.headers.sessionid;
            orderService.getOrderInfoDetails(orderId, sessionId)
                .then(
                    result => {
                        const resutData = JSON.parse(result);
                        if (resutData.result[0].error) {
                            res.status(500).send(resutData.result[0].error);
                        } else {
                            const recordData = resultData.result[0].row[0];
                            const fileName = recordData.fname;
                            const filePath = recordData.fpath;
                            const fullPath = path.join(filePath, fileName);
                            wasabiService.getSignedUrl(fullPath)
                                .then(
                                    response => {
                                        const responseMessage = {
                                            'fileName': fileName,
                                            'signedUrl': response
                                        }
                                        res.status(200).send(responseMessage);
                                    }
                                )
                                .catch(
                                    reason => {
                                        logger.error(`SignedUrl Error : ${reason}`);
                                        res.status(500).send(reason);
                                    }
                                )
                        }
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    downloadAllFiles: async (req, res, next) => {
        const responseMessage = {
            status: false,
            message: '',
            signedUrl: '',
            fileName: ''
        };
        try {
            const fileObj = req.body;
            logger.info(`File Object ::: ${JSON.stringify(fileObj)}`);
            wasabiService.downloadZipFile(fileObj)
                .then(
                    resultData => {
                        logger.info(`Result Data = ${resultData}`);
                        wasabiService.getSignedUrl(resultData)
                            .then(
                                response => {
                                    logger.info(`Response Value::: ${response}`)
                                    responseMessage.status = true;
                                    responseMessage.message = "Download details populated successfully.";
                                    responseMessage.fileName = resultData;
                                    responseMessage.signedUrl = response;
                                    res.status(200).send(responseMessage);
                                }
                            )
                            .catch(
                                reason => {
                                    logger.error(`SignedUrl Error : ${reason}`);
                                    responseMessage.status=false;
                                    responseMessage.message = reason;
                                    res.status(500).send(responseMessage);
                                }
                            );
                       // res.status(200).send(responseMessage);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error Reason :: ${reason}`);
                        res.status(500).send(responseMessage);
                    }
                )

        } catch (err) {
            logger.error(`Download Error ::: ${err}`);
        }
    },
    getFullSignedUrl: async (req, res, next) => {
        try {
            const fullPath = req.query.fullPath;

            wasabiService.getSignedUrl(fullPath)
                .then(
                    response => {
                        const responseMessage = {
                            'fileName': path.basename(fullPath),
                            'signedUrl': response
                        }
                        res.status(200).send(responseMessage);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`SignedUrl Error : ${reason}`);
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
};

module.exports = s3Module;
