const logger = require('../utilities/logger');
const commonService = require('../services/commonServices');
const orderService = require('../services/OrderService');

const orderDataModule = {
    getOrderData: async (req, res, next) => {
        try {
            const listType = req.query.listType;

            const userCode = req.query.userCode || '';

            const sessionId = req.headers.sessionid;

            const response = await commonService.loadOrderData(listType, userCode, sessionId);
            const responseData = response.data;
            if (responseData.result[0].error) {
                res.status(500).send('{"errorStatus":"Failed","message":"Unable to Load the data"}');
            } else {
                res.status(200).send(responseData.result[0].row);
            }
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getOrderDetails: async (req, res, next) => {
        try {
            const orderId = req.query.orderId;

            const sessionId = req.headers.sessionid;

            orderService.getOrderDetails(orderId, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        if (resultData.result[0].error) {
                            res.status(500).send(`{"errorStatus":"Failed","message":"Unable to fetch the record error ${JSON.stringify(resultData.result[0].error)}"}`);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    })
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateSmileOrderData: async (req, res, next) => {
        const columnObj = req.body;
        try {
            const response = await orderService.updateSmileOrder(columnObj);
            const responseData = response.data;
            if (responseData.result[0].error) {
                res.status(500).send('{"errorStatus":"Failed","message":"Unable to Update data"}');
            } else {
                res.status(200).send(responseData.result[0].message[0]);
            }
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getRxOrderDetails: async (req, res, next) => {
        try {
            const orderId = req.query.orderId;

            const sessionId = req.headers.sessionid;

            orderService.getRxOrderDetails(orderId, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    })
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getSearchOrderDetails: async (req, res, next) => {
        try {
            const searchObj = req.body;
            const sessionId = req.headers.sessionid;

            orderService.getSearchOrderDetails(searchObj, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`gerSearchOrderDetails error Reason : ${reason}`);
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = orderDataModule;
