const logger = require('../utilities/logger');
const messageService = require('../services/MessageService');
const commonService = require('../services/commonServices');
const MessageModule = {
    sendMessage: async (req, res, next) => {
        try {
            const messageObject = req.body;
            const sessionId = req.headers.sessionid;
            messageService.sendMessage(messageObject, sessionId)
                .then(
                    response => {
                        const resultData = JSON.parse(response);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getOrderMessage: async (req, res, next) => {
        try {
            const messageType = req.query.messageType;
            const pid = req.query.pid;

            const sessionId = req.headers.sessionid;

            const orderParam = {
                "pmessagetype": messageType,
                "pid": pid
            }
            messageService.getOrderMessage(orderParam, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        }
                        else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    })
                .catch(
                    reason => {
                        res.status(500).send(reason);
                    }
                )
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    postFiles:  async (req, res, next) => {
        try {
            const fileObj = req.body;
            const sessionId = req.headers.sessionid;

            commonService.postFiles(fileObj, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);

                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        } else {
                            res.status(200).send(resultData.result[0].message[0]);
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Post FIle Error ${reason}`);
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getFiles: async (req, res, next) => {
        try {

            const orderId = req.query.orderId;
            const sessionId = req.headers.sessionid;

            commonService.getFiles(orderId, sessionId)
                .then(
                    result => {
                        const resultData = JSON.parse(result);

                        if (resultData.result[0].error) {
                            res.status(500).send(resultData.result[0].error);
                        } else {
                            res.status(200).send(resultData.result[0].row);
                        }
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Get FIle Error ${reason}`);
                        res.status(500).send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = MessageModule;
