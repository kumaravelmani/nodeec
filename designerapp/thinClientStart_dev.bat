ECHO ""
CALL ECHO "Starting the application"
CALL pm2 stop ecosystem.config.js
CALL pm2 delete ThinClientApp
CALL git stash
CALL git pull
:: Required for First Time Installation
:: CALL npm install --global --production windows-build-tools --vs2015
:: CALL npm config set msvs_version 2015 –global
:: CALL npm config set python python2.7
CALL npm install
CALL pm2 start ecosystem.config.js --env development && PAUSE
