module.exports = {
    apps: [{
        name: "ThinClientApp",
        script: "./bin/www",
        watch: true,
        instances: 2,
        autorestart: true,
        max_memory_restart: '1G',
        env: {
            "PORT": 3020,
            "NODE_ENV": "development",
            "NODE_ORACLEDB_USER": "easyconnect",
            "NODE_ORACLEDB_PASSWORD": "log",
            "NODE_ORACLEDB_CONNECTIONSTRING": "192.168.12.8/orcl",
            "NODE_ORACLEDB_EXTERNALAUTH": false,
            "JWT_SECRET": "djf856%%*75687^%*%^*ytw96tr^^&%&^%&^cwecb785rt34t5",
            "API_URL": "http://192.168.12.8:3018/",
            "LOG_URL": "http://192.168.12.8/mylabconnectx7scripts/",
            "STLVIEWER_URL": "http://192.168.12.8:3022/stlviewer"
        },
        env_production: {
            "PORT": 3012,
            "NODE_ENV": "production",
            "NODE_ORACLEDB_USER": "easyconnect",
            "NODE_ORACLEDB_PASSWORD": "log",
            "NODE_ORACLEDB_CONNECTIONSTRING": "orclcloud",
            "NODE_ORACLEDB_EXTERNALAUTH": false,
            "JWT_SECRET": "djf856%%*75687^%*%^*ytw96tr^^&%&^%&^cwecb785rt34t5",
            "API_URL": "https://easydentconnect.com:3018/",
            "LOG_URL": "https://easydentconnect.com/EasyConnectScripts/",
            "STLVIEWER_URL": "https://easydentconnect.com/stlviewer/"
        }
    }]
}