const db = require("../db/oracledb.js");
const oracledb = require('oracledb');
const logger = require('../utility/logger');
const CircularJSON = require('circular-json');

async function getMasterConfiguration() {
    const procSql = `
 BEGIN
 get_masters (:o);
 END;`;
    const bindParams = {
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 20000 },
    }
    logger.info("INFO: Service - Entered new order Service");
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from pull order procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from get_masters PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from get_master PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

module.exports = MasterConfigService = {
    getMasterConfiguration
}