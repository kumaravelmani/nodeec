const db = require("../db/oracledb.js");
const oracledb = require('oracledb');
const logger = require('../utility/logger');
const CircularJSON = require('circular-json');

async function pullOrderfromQueue(pusername) {
    const procSql = `
 BEGIN
 pullOrderIOSQC (:o,:pusername);
 END;`;
    const bindParams = {
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 },
        pusername: pusername
    }
    logger.info("INFO: Service - Entered new order Service");
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from pull order procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function UpdateCaseDetails(params) {
    const procSql = `
 BEGIN
 rxDetailsUpdate (:op_result,:pUserName,:ds_rxDetailId,:pcaseno,:pDesignType,:pNotations,:pProduct,:pModel);
 END;`;
    const bindParams = {
        op_result: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 },
        pUserName: params.pUserName,
        ds_rxDetailId: params.ds_rxDetailId,
        pcaseno: params.pcaseno || '',
        pDesignType: params.pDesignType,
        pNotations: params.pNotations,
        pProduct: params.pProduct || '',
        pModel: params.pModel || ''
    }
    logger.info(`INFO: Service - Entered new order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from status update procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.op_result)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function searchOrderResult(params) {
    const procSql = `
 BEGIN
 get_orderdetail (:op_result,:pcaseno);
 END;`;
    const bindParams = {
        op_result: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 },
        pcaseno: params.caseno
    }
    logger.info(`INFO: Service - Entered search order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving search order data from get_orderdetail procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from search order DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from search order DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.op_result)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function updateStatus(params) {
    const procSql = `
 BEGIN
 orderStatusUpdate (:pcaseno,:pstatus,:premarks,:pupdatedby,:pupdatedon,:o);
 END;`;
    const bindParams = {
        pcaseno: params.caseno,
        pstatus: parseInt(params.status),
        premarks: params.comments || '',
        pupdatedby: params.updatedby,
        pupdatedon: params.updatedon,
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    };

    logger.info(`INFO: Service - Entered  order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from status update procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function updateStatusIosqc(params) {
    const procSql = `
 BEGIN
 orderStatusUpdate_iosqc (:pcaseno,:pstatus,:premarks,:pupdatedby,:pupdatedon,:pQcRating,:pQcReason,:o);
 END;`;
    const bindParams = {
        pcaseno: params.caseno,
        pstatus: parseInt(params.status),
        premarks: params.comments || '',
        pupdatedby: params.updatedby,
        pupdatedon: params.updatedon,
        pQcRating: params.qcrating || '',
        pQcReason: params.qcreason || '',
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    };

    logger.info(`INFO: Service - Entered IOSQC order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from IOSQC status update procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function updateStatusDesigner(params) {
    const procSql = `
 BEGIN
 orderStatusUpdate_designer (:pcaseno,:pstatus,:premarks,:pupdatedby,:pupdatedon,:pcorrection_done,:ptool_used,:o);
 END;`;
    const bindParams = {
        pcaseno: params.caseno,
        pstatus: parseInt(params.status),
        premarks: params.comments || '',
        pupdatedby: params.updatedby,
        pupdatedon: params.updatedon,
        pcorrection_done: params.correction || '',
        ptool_used: params.tool || '',
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    };

    logger.info(`INFO: Service - Entered Designer order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from Designer status update procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

async function updateStatusFinalQc(params) {
    const procSql = `
 BEGIN
 orderStatusUpdate_finalqc (:pcaseno,:pstatus,:premarks,:pupdatedby,:pupdatedon,:pQcRating,:passigned_to,:prx,:pmargin,:pqdirection,:pcontact,:pocclusal,:pshape,:psize,:o);
 END;`;
    const bindParams = {
        pcaseno: params.caseno,
        pstatus: parseInt(params.status),
        premarks: params.comments || '',
        pupdatedby: params.updatedby,
        pupdatedon: params.updatedon,
        pQcRating: params.qcrating || '',
        passigned_to: params.assigned_to || '',
        prx: params.rx || '',
        pmargin: params.margin || '',
        pqdirection: params.qdirection || '',
        pcontact: params.contact || '',
        pocclusal: params.occlusal || '',
        pshape: params.shape || '',
        psize: params.size || '',
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    };

    logger.info(`INFO: Service - Entered Final QC order Service::: ${JSON.stringify(bindParams)}`);
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving order data from Final Qc status update procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams, // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Error from DB PROC ::: ${err}`);
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection); // RELEASE CONNECTION
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`);
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD-result.outBinds.o.result[0].data
                        }
                    }
                );
            }
        });
    });
}

module.exports = OrderService = {
    pullOrderfromQueue,
    searchOrderResult,
    UpdateCaseDetails,
    updateStatus,
    updateStatusIosqc,
    updateStatusDesigner,
    updateStatusFinalQc

};