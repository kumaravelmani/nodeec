//////////////////////////////
// REQUIRE RELEVANT MODULES //
//////////////////////////////
var db          = require("../db/oracledb.js");
var oracledb    = require('oracledb');
var sql         = "";

///////////////////////////
// RETRIEVE CURRENT DATE //
///////////////////////////
module.exports.getCurDate = function(callback) {

    sql = "SELECT CURRENT_DATE FROM DUAL";
    db.doConnect(function(err, connection){
        console.log("INFO: Database - Retrieving CURRENT_DATE FROM DUAL");
        if (err) {
            console.log("ERROR: Unable to get a connection ");
            return callback(err);
        } else {
            db.doExecute(
                connection, sql
                , {} // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                , function(err, result) {
                    if (err) {
                        db.doRelease(connection);     // RELEASE CONNECTION
                        return callback(err);                 // ERROR
                    } else {
                        db.doRelease(connection);     // RELEASE CONNECTION
                        return callback(err, result.rows);    // ALL IS GOOD
                    }
                }
            );
        }
    });
}
