const db = require("../db/oracledb.js");
const oracledb = require('oracledb');
const logger = require('../utility/logger');
const CircularJSON = require('circular-json');

async function getWidgetdata(roleData) {
    const procSql = `
        BEGIN
         proc_dbeddc(:i,:o);
       END;
    `;
    const bindParams = {
        i: roleData,
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    }
    logger.info("INFO: Service - Entered DashBoard Service");
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving Widget data from procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    ,
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection);
                            logger.info(`Error from DB PROC ::: ${err}`); // RELEASE CONNECTION
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection);
                            logger.info(`Response from DB PROC ::: ${JSON.stringify(result)}`); // RELEASE CONNECTION
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD
                        }
                    }
                );
            }
        });
    });
}

async function iosqcreasons() {
    const procSql = `
        BEGIN
        get_iosqcreasons(:o);
       END;
    `;
    const bindParams = {
        o: { type: oracledb.JSON, dir: oracledb.BIND_OUT, maxSize: 10000 }
    }
    logger.info("INFO: Service - Entered reasons Service");
    return new Promise((resolve, reject) => {
        db.doConnect(function(err, connection) {
            logger.info("INFO: Database - Retrieving reason data from procedure");
            if (err) {
                logger.error("ERROR: Unable to get a connection ");
                return reject(err);
            } else {
                db.doExecute(
                    connection, procSql, bindParams // PASS BIND PARAMS IN HERE - SEE ORACLEDB DOCS
                    ,
                    function(err, result) {
                        if (err) {
                            db.doRelease(connection);
                            logger.info(`Error from DB PROC ::: ${err}`); // RELEASE CONNECTION
                            return reject(err); // ERROR
                        } else {
                            // result == 11,22,33
                            db.doRelease(connection);
                            logger.info(`Response from get_iosqcreasons PROC ::: ${JSON.stringify(result)}`); // RELEASE CONNECTION
                            return resolve(JSON.parse(JSON.stringify(CircularJSON.stringify(result.outBinds.o)))); // ALL IS GOOD
                        }
                    }
                );
            }
        });
    });
}
module.exports = widgetServices = {
    getWidgetdata,
    iosqcreasons
};