module.exports = {
    user          : process.env.NODE_ORACLEDB_USER || "easyconnect",

    password      : process.env.NODE_ORACLEDB_PASSWORD,

    connectString : process.env.NODE_ORACLEDB_CONNECTIONSTRING || "localhost/xe",

    externalAuth  : process.env.NODE_ORACLEDB_EXTERNALAUTH ? true : false
};
