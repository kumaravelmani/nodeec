const widgetServices = require('../services/widgetServices');
const logger = require('../utility/logger');

const widgetModule = {
    getWidgetdata: async(req, res, next) => {

        //getWidgetDate?role=IOSQC
        try {

            const prole = req.query.role;
            logger.info(`JSON ::: ${JSON.stringify(prole)}`);

            widgetServices.getWidgetdata(prole)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : Widget data::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : Widget data ::: ${reason}`);
                        res.send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    getqcreasons: async(req, res, next) => {

        try {
            widgetServices.iosqcreasons()
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : QC reasons data::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API :  QC reasons data ::: ${reason}`);
                        res.send(reason);
                    }
                );
        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    }
}

module.exports = widgetModule;