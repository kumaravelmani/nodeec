const OrderServices = require('../services/orderServices');
const commonServices = require('../services/commonServices');
const logger = require('../utility/logger');

const orderModule = {
    newOrder: async(req, res, next) => {
        try {
            const pusername = req.query.userName;

            OrderServices.pullOrderfromQueue(pusername)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : NEWORDER ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : NEWORDER ::: ${reason}`);
                        res.status(500).send(`Problem with getting a New order ::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateCaseDetails: async(req, res, next) => {
        try {

            var params = {
                pUserName: req.body.userName,
                ds_rxDetailId: req.body.ds_detailsId,
                pcaseno: req.body.caseno,
                pDesignType: req.body.designType,
                pNotations: req.body.notation,
                pProduct: req.body.product,
                pModel: req.body.model
            }

            OrderServices.UpdateCaseDetails(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS ::: ${reason}`);
                        res.status(500).send(`Problem with updating a status ::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    searchOrderResult: async(req, res, next) => {
        try {

            var params = {
                caseno: req.query.caseno
            }

            OrderServices.searchOrderResult(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS ::: ${reason}`);
                        res.status(500).send(`Problem with updating a status ::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateStatus: async(req, res, next) => {
        try {

            var params = {
                caseno: req.body.caseno,
                status: req.body.status,
                comments: req.body.comments,
                updatedby: req.body.updatedby,
                updatedon: commonServices.getDate(new Date) // created as global date format used through out the appliction
            };

            OrderServices.updateStatus(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS ::: ${reason}`);
                        res.status(500).send(`Problem with updating a status ::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateStatusIosqc: async(req, res, next) => {
        try {

            var params = {
                caseno: req.body.caseno,
                status: req.body.status,
                comments: req.body.comments,
                updatedby: req.body.updatedby,
                updatedon: commonServices.getDate(new Date), // created as global date format used through out the appliction
                qcrating: req.body.qcrating,
                qcreason: req.body.qcreason
            };

            OrderServices.updateStatusIosqc(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS IOSQC ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS IOSQC ::: ${reason}`);
                        res.status(500).send(`Problem with updating a status IOSQC ::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateStatusDesigner: async(req, res, next) => {
        try {

            var params = {
                caseno: req.body.caseno,
                status: req.body.status,
                comments: req.body.comments,
                updatedby: req.body.updatedby,
                updatedon: commonServices.getDate(new Date), // created as global date format used through out the appliction
                correction: req.body.correction,
                tool: req.body.tool
            };

            OrderServices.updateStatusDesigner(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS Designer ::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS Designer::: ${reason}`);
                        res.status(500).send(`Problem with updating a status Designer::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },
    updateStatusFinalQc: async(req, res, next) => {
        try {

            var params = {
                caseno: req.body.caseno,
                status: req.body.status,
                comments: req.body.comments,
                updatedby: req.body.updatedby,
                updatedon: commonServices.getDate(new Date), // created as global date format used through out the appliction
                qcrating: req.body.qcrating,
                assigned_to: req.body.assignedTo,
                rx: req.body.rx,
                margin: req.body.margin,
                qdirection: req.body.qdirection,
                contact: req.body.contact,
                occlusal: req.body.occlusal,
                shape: req.body.shape,
                size: req.body.size
            };

            OrderServices.updateStatusFinalQc(params)
                .then(
                    result => {
                        const resultData = JSON.parse(result);
                        logger.info(`Response API : UPDATE STATUS Final Qc::: ${JSON.stringify(resultData)}`);
                        res.status(200).send(resultData);
                    }
                )
                .catch(
                    reason => {
                        logger.error(`Error API : UPDATE STATUS Final Qc::: ${reason}`);
                        res.status(500).send(`Problem with updating a status Final Qc::: ${reason}`);
                    }
                );

        } catch (err) {
            logger.error(`Error ${err}`);
            next(err);
        }
    },

};

module.exports = orderModule;