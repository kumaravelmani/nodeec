const express = require('express');
const router = express.Router();
const logger = require('../utility/logger');

/* GET home page. */
router.get('/', function(req, res, next) {
    res.render('dashboard');
});

module.exports = router;
