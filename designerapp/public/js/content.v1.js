"use strict";

if (sessionStorage.getItem("token") === null) {
    window.location.href = "/login";
}

const config = JSON.parse(sessionStorage.getItem('config'));
const masterDataConfig = JSON.parse(sessionStorage.getItem('masterConfig')); //temp store
const masterData = JSON.parse(masterDataConfig).result[0].data;
const userDetails = JSON.parse(sessionStorage.getItem("UserDetails"));
const statusConfig = JSON.parse(sessionStorage.getItem("statusConfig"));
const apiToken = sessionStorage.getItem("token");

const sessionid = userDetails.s;
const API_URL = config.API_URL;
var STLVIEWER_URL = config.STLVIEWER_URL;

const statusText = [
    'Order Created',
    'Order Accepted',
    'Scan Validation Started',
    'Scan Validation Completed',
    'Received for Design',
    'Design Started',
    'Design Completed',
    'Received for QC',
    'Quality Check Started',
    'Quality Check Completed',
    'Order Completed',
    'Order on Hold',
    'Order In Progress',
    'Order Returned'
];

var orderData;
var tm;
var str;
var h = 0; // hours
var m = 0; //minutes
var s = 0; //seconds
var timerStore; //session-storage value of str, h, m,s
var timerStatusStore; // currebt status of icon -play/pause
var newStatus;
// sessionStorage.setItem('TimerStore', timerStore);
// sessionStorage.setItem('TimerStatusStore', timerStatusStore);

var orderStatusStore;
// sessionStorage.setItem('OrderStatusStore', orderStatusStore);
var caseNo;
var allFiles;

var messageReload;

$(document).ready(function() {
    if (sessionStorage.getItem('orderOnHand')) {
        var orderData = JSON.parse(sessionStorage.getItem('orderOnHand'));
        if (sessionStorage.getItem('OrderStatusStore')) {
            constructNewOrderData(orderData, sessionStorage.getItem('OrderStatusStore'));
        } else {
            constructNewOrderData(orderData, statusText[1]);
        }
    } else if (sessionStorage.getItem('viewOrder')) {
        var orderData = JSON.parse(sessionStorage.getItem('viewOrder'));
        if (sessionStorage.getItem('OrderStatusStore')) {
            constructViewOrder(orderData, sessionStorage.getItem('OrderStatusStore'));
        } else {
            constructViewOrder(orderData, statusText[0]);
        }
    } else {
        NoDataAvailable()
    }

    if (userDetails.ugroup === 'IOSQC') {
        $('#return-for-designer').hide();

        $('#final-file-option').hide();
        $('#filetype-easysmile').hide();
        $('#filetype-easyvalidate').hide();
    } else if (userDetails.ugroup === 'Designer') {

        $('#return-for-designer').show();
    } else if (userDetails.ugroup === 'Designer QC') {
        $('#return-for-designer').hide();

        $('#final-file-option').show();
        $('#filetype-easysmile').hide();
        $('#filetype-easyvalidate').hide();
    }
    // inside document.ready
    //-------------------------------//view stl file-----------------------//Download- Initial file - functionality//---------------------------------------------------------//

    $('#filesDetails').on("click", "#stlviewing", function() {
        window.open(STLVIEWER_URL);
    });

    $('#filesDetails').on("click", "#initialFile", function() {

        $("#alertSucInOrder").remove();
        $("#alertSucOrder").append('<div id="alertSucInOrder" class="alert alert-success alertSuccessOrder alert-dismissible fade show" role="alert">Initial file is getting download <button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');

        var orderid = $("#caseno").val();

        $.ajax({
            url: `${API_URL}downloadFile?orderId=${orderid}`,
            method: 'GET',
            //JQuery to expect a blob response type.
            xhrFields: {
                responseType: 'blob'
            },
            headers: {
                sessionid: sessionid
            },
            success: function(response) {

                alertPopMessage('success', '', 'Initial file downloaded', 4000, '');

                var file = new Blob(
                    [response.data], { type: 'application/zip' });
                var fileURL = URL.createObjectURL(file);
                var a = document.createElement('a');
                a.href = fileURL;
                a.download = $("#fileName").val();
                document.body.append(a);
                a.click();
                a.remove();
                window.URL.revokeObjectURL(fileURL);

            },
            error: function(xhr, status, error) {
                alertPopMessage('danger', 'Error', `Problem with downloading Initial file:${error}`, 4000, '');

            }

        });
    });

    $("#submitCaseDetails").click(function() {

        var orderid = $("#caseno").val();
        var ds_detailsId = 0;
        var editTypeCaseDetails = $('.modal-body .modal-data-source span').text();

        if (editTypeCaseDetails === 'add') {
            ds_detailsId = 0;
        } else if (editTypeCaseDetails === 'edit') {
            ds_detailsId = $('.modal-body .modal-data-detailid span').text();
        }

        var casedetailsUpdate = {
            userName: userDetails.USERNAME,
            ds_detailsId: ds_detailsId,
            caseno: $("#caseno").val(),
            designType: $("#designTypeEdit").val(),
            notation: $("#notationsEdit").val(),
            product: $("#productEdit").val(),
            model: $("#modelEdit").val()
        };

        $.ajax({
            url: '/updateCaseDetails',
            type: 'POST',
            data: casedetailsUpdate,
            headers: {
                'x-access-token': apiToken
            },
            success: function(result, status, xhr) {

                var caseDetailsResp = JSON.parse(result);

                if (caseDetailsResp.result[0].status.code !== '200') {

                    alertPopMessage('danger', 'Error', `Problem with ${editTypeCaseDetails}ing the case details`, 4000, '');
                } else {
                    alertPopMessage('success', 'Success', 'Case Details updated successfully', 4000, '');
                    getRxOrderDetails();
                }
            },
            error: function(xhr, status, error) {

                if (xhr.status === 401) {
                    var messageUnathorized = 'Unauthorized! session expired, Please login again';
                    alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

                } else {
                    var messageError = xhr.responseText;
                    alertPopMessage('danger', 'Error', messageError, 4000, '');
                }
            }
        });

    });

    $("#neworderbtn").click(function() {

        NoDataAvailable();

        $.ajax({
            type: 'GET',
            url: `/newOrder?userName=${userDetails.USERNAME}`,
            headers: {
                'x-access-token': apiToken
            },
            success: function(result, status) {

                var orderData = JSON.parse(result);
                // var newStatus = statusText[1];
                if (orderData.result[0].status.code !== '200') {
                    console.log(orderData.result[0].status.msg)
                } else {
                    sessionStorage.setItem('orderOnHand', JSON.stringify(orderData));

                    // alertPopMessage('success', 'Success', newStatus, 4000, '');

                    orderStatusStore = sessionStorage.getItem('OrderStatusStore');
                    if (orderStatusStore) {
                        newStatus = orderStatusStore;
                    } else {
                        newStatus = statusText[orderData.result[0].data.status];
                    }

                    constructNewOrderData(orderData, newStatus);
                    $('#timerbut').click();

                }
            },
            error: function(xhr, status, error) {

                if (xhr.status === 401) {
                    var messageUnathorized = 'Unauthorized! session expired, Please login again';
                    alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

                } else {
                    var messageError = xhr.responseText;
                    alertPopMessage('danger', 'Error', messageError, 4000, '');
                }
            }
        });
    });

    $("#searchSubmit").click(function() {

        if (sessionStorage.getItem('orderOnHand')) {
            alertPopMessage('danger', 'Error', 'Checkout the current order to view new order', 4000, '');
        } else {

            var paramCaseno = $('#searchCase').val().split(' -')[0];

            $.ajax({
                type: 'GET',
                url: `/searchOrder?caseno=${paramCaseno}`,
                headers: {
                    'x-access-token': apiToken
                },
                success: function(result, status) {
                    $('#searchCase').val('');
                    var orderData = JSON.parse(result);
                    // var newStatus = statusText[1];
                    if (orderData.result[0].status.code !== '200') {
                        console.log(orderData.result[0].status.msg)
                    } else {
                        sessionStorage.setItem('viewOrder', JSON.stringify(orderData));

                        // alertPopMessage('success', 'Success', newStatus, 4000, '');

                        orderStatusStore = sessionStorage.getItem('OrderStatusStore');
                        if (orderStatusStore) {
                            newStatus = orderStatusStore;
                        } else {
                            newStatus = statusText[orderData.result[0].data.status];
                        }

                        constructViewOrder(orderData, newStatus)

                    }
                },
                error: function(xhr, status, error) {
                    $('#searchCase').val('');
                    if (xhr.status === 401) {
                        var messageUnathorized = 'Unauthorized! session expired, Please login again';
                        alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

                    } else {
                        var messageError = xhr.responseText;
                        alertPopMessage('danger', 'Error', messageError, 4000, '');
                    }
                }
            });
        }
    });

    $('#sendAttachmentIcon').click(function() {
        $('#myAttachment').click();
    });

    $('#myInput').keypress(function(e) {
        var key = e.which;
        if (key == 13) // the enter key code
        {
            $('#submitMessage').click();
            return false;
        }
    });

    $('#submitMessage').click(function() {
        var input = $("#myInput").val();
        var myatt = $('#myAttachment').val();
        var attachments = [];

        $("#sendAttachmentIcon-spinner").removeClass("display-none");
        $("#submitMessage").addClass("display-none");

        if (myatt) {
            messageFileAttachement(input, attachments);
        } else {
            messageRender(input, attachments);
        }
    });

    $('#nav-message').click(function() {
        scrollToBottom();
    });

    $('#nav-message-tab').click(function() {
        scrollToBottom();
    });

    $('#sendFileAttachement').click(function() {

        caseNo = $("#caseno").val();

        var fd = new FormData();
        var files = $('#myFileAttachement')[0].files[0];
        var fileExt = $('#myFileAttachement').val().split('.')[1];

        if (files) {
            $(".custom-spinner").removeClass("display-none");
            $("#sendFileAttachement .spanTag").html('Uploading...');
            $("#sendFileAttachement").attr("disabled", true);

            fd.append('filetoattach', files);

            var filePost = $("#filePost").val();
            var serviceType = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data.servicetype : '0';
            var orderonHandFile = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data : null;
            // to-do send msg default 

            var extFlag = false;
            var extFlagFinalfileCheck = false;

            switch (filePost) {
                case "STL_File":
                    if (fileExt == 'stl') extFlag = true;
                    break;

                case 'Quality_Image':
                    if (['png', 'jpg', 'jpeg'].includes(fileExt)) extFlag = true;
                    break;

                case 'Others':
                    extFlag = true;
                    break;

                case 'Final_Smile':
                    if (['png', 'jpg', 'jpeg', 'PNG'].includes(fileExt)) extFlag = true;
                    break;

                case 'Final_STL':
                    if (fileExt == 'stl') {
                        extFlag = true
                    };
                    break;

                case 'Final_Scan':
                    if (fileExt == 'zip') {

                        if ((orderonHandFile.fileName === files.name) && (orderonHandFile.fileSize < files.size)) {
                            extFlagFinalfileCheck = false;
                            extFlag = true;
                        } else {
                            extFlag = false;
                            extFlagFinalfileCheck = true;
                        }
                    } else {
                        extFlag = false;
                    }

                    break;
                default:
                    extFlag = false;

            }

            if (filePost && caseNo && $('#myFileAttachement')[0].files.length && extFlag) {
                $.ajax({
                    url: `${API_URL}attachMessageFile`,
                    type: 'POST',
                    data: fd,
                    contentType: false,
                    processData: false,
                    headers: {
                        orderid: caseNo,
                        sourcetype: filePost,
                        dsorderid: '',
                        sessionid: sessionid
                    },
                    success: function(response) {
                        alertPopMessage('success', 'Success', 'File got uploaded successfully', 4000, '');
                        fileTableRender();
                        $(".custom-spinner").addClass("display-none");
                        $("#myFileAttachement").val('');
                        $("#sendFileAttachement .spanTag").html('Upload');
                        $("#sendFileAttachement").attr("disabled", false);
                    },
                    error: function(error) {
                        console.log(error)
                        $(".custom-spinner").addClass("display-none");
                        $("#sendFileAttachement .spanTag").html('Upload');
                        $("#sendFileAttachement").attr("disabled", false);
                    }
                });
            } else {
                if (extFlagFinalfileCheck) {
                    alertPopMessage('danger', 'Error', 'Please upload the Final file with same name of Initial file', 4000, '');
                    $(".custom-spinner").addClass("display-none");
                    $("#sendFileAttachement .spanTag").html('Upload');
                    $("#sendFileAttachement").attr("disabled", false);
                } else {
                    alertPopMessage('danger', 'Error', 'Please select valid file format & file type', 4000, '');
                    $(".custom-spinner").addClass("display-none");
                    $("#sendFileAttachement .spanTag").html('Upload');
                    $("#sendFileAttachement").attr("disabled", false);
                }
            }
        } else {

            alertPopMessage('danger', 'Error', 'Please select a file', 4000, '');
        }
    });

    $('#sendAttachment').click(function() {
        messageFileAttachement();
    });

    //-------------------------------------------------//------Save---- Save and get next order functionality-//-------------------------------------------------//
    //for save button
    $('#qcratingorder').on('change', function() {
        if ($("#qcratingorder").val() === "Bad") {
            iosqureason($("#qcreason"));
            $("#qcreason").css('display', 'block');
            $("#holdreasonpart").css('display', 'block');
        } else {
            $("#qcreason").val('');
            $("#qcreason").css('display', 'none');
            $("#holdreasonpart").css('display', 'none');
        }
        //to get reasons for qc
    });
    $('#assignedtoCheckout').hide();
    $('#overallstatusfinal').on('change', function() {
        if ($("#overallstatusfinal").val() === 'Rejected') {
            $('#assignedtofinal').val('self');
            $('#assignedtoCheckout').show();
        } else {
            $('#assignedtofinal').val('');
            $('#assignedtoCheckout').hide();
        }
        //to get reasons for qc
    });

    $('#editCaseDetails').on('hidden.bs.modal', function(e) {

        $("#designTypeEdit").val('');
        $("#notationsEdit").val('');
        $("#productEdit").val('');
        $("#modelEdit").val('');

        $('.modal-body .modal-data-detailid span').text('');
    });

    $('#editCaseDetails').on('show.bs.modal', function(event) {
        var button = $(event.relatedTarget);
        var sourceClick = button.data('src');

        var modal = $(this)
        modal.find('.modal-body .modal-data-source').html(`<span style="display:none">${sourceClick}</span>`);
    });

    $('#checkoutBut').click(function() {

        switch (userDetails.ugroup) {
            case 'IOSQC':
                $('#checkout-for-iosqc').modal('show');
                break;
            case 'Designer':
                $('#checkout-for-designer').modal('show');
                break;
            case 'Designer QC':
                $('#checkout-for-finalqc').modal('show');
                break;
        }

    });
    $('#returnBut').click(function() {

        switch (userDetails.ugroup) {
            case 'Designer':
                $('#returnModal-designer').modal('show');
                iosqureason($("#retReasonDes"));
                break;
                // case 'Designer QC':
                //     $('#checkout-for-finalqc').modal('show');
                //     break;
        }

    });

    $('#checkout-for-iosqc').on('hidden.bs.modal', function(e) {

        $("#qcratingorder").val('');
        $("#qcreason").val('');
        $(".message-text-checkout-iosqc").val('');

        $("#qcreason").css('display', 'none');
        $("#holdreasonpart").css('display', 'none');

        $('#text-checkout-iosqc').text('');

    });
    $('#checkout-for-designer').on('hidden.bs.modal', function(e) {

        $("#correctionsDes").val('N')
        $("#toolUsed").val('3Shape')
        $("#message-text-checkout-designer").val('');

    });
    $('#checkout-for-finalqc').on('hidden.bs.modal', function(e) {

        $("#message-text-checkout-finalqc").val('');
        $("#overallstatusfinal").val('');
        $('#assignedtofinal').val('');
        $('#text-checkout-final').text('');

    });
    $('#returnModal-designer').on('hidden.bs.modal', function(e) {

        $("#retReasonDes").val('0');
        $(".message-text-returnDes").val('');
        $('#text-return-designer').text('');

    });

    $("#save-checkout-for-iosqc").click(function() {

        if ($("#qcratingorder").val() !== null) {
            $('#text-checkout-iosqc').text('');
            var fields = {
                caseno: $("#caseno").val(),
                message: $(".message-text-checkout-iosqc").val() || '',
                qcrating: $("#qcratingorder").val() || '',
                qcreason: $("#qcreason").val() || ''
            };

            $('#checkout-for-iosqc').modal('hide');
            if (fields.qcrating == 'Bad') {
                if ($("#qcreason").val() !== '') {

                    $('#text-checkout-iosqc').text('');
                    checkoutOrderIosqc(statusConfig.onReturned, fields);
                } else {
                    $('#text-checkout-iosqc').text('*Please select a reason');
                }
            } else {
                checkoutOrderIosqc(statusConfig.onCompleted, fields);
            }
        } else {
            $('#text-checkout-iosqc').text('*Please select a rating');
        }

    });

    $("#save-checkout-for-designer").click(function() {

        var fields = {
            caseno: $("#caseno").val(),
            correction: $("#correctionsDes").val() || '',
            tool: $("#toolUsed").val() || '',
            message: $("#message-text-checkout-designer").val() || ''
        };
        $('#checkout-for-designer').modal('hide');

        var serviceType = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data.serviceType : '0';
        switch (serviceType) {
            case '0':
                checkoutOrderDesigner(statusConfig.onCompleted, fields);
                break;
            case '1':
                var finalSmilefilecheck = allFiles.filter(function(file) {
                    return file.filetype === 'Final Smile';
                });
                if (finalSmilefilecheck.length > 0) {
                    checkoutOrderDesigner(statusConfig.onCompleted, fields);
                } else {
                    alertPopMessage('danger', 'Error', 'Please upload Final Smile before submitting', 4000, '');
                }
                break;
            case '2':
                var finalSmilefilecheck = allFiles.filter(function(file) {
                    return file.filetype === 'Final Smile';
                });
                var finalScanfilecheck = allFiles.filter(function(file) {
                    return file.filetype === 'Final Scan';
                });
                if (finalSmilefilecheck.length > 0 && finalScanfilecheck.length > 0) {
                    checkoutOrderDesigner(statusConfig.onCompleted, fields);
                } else {
                    alertPopMessage('danger', 'Error', 'Please upload Final Smile & Final Scan before submitting', 4000, '');
                }
                break;
            case '3':
                var finalStlfilecheck = allFiles.filter(function(file) {
                    return file.filetype === 'Final STL';
                });
                var finalScanfilecheck = allFiles.filter(function(file) {
                    return file.filetype === 'Final Scan';
                });
                if (finalStlfilecheck.length > 0 && finalScanfilecheck.length > 0) {
                    checkoutOrderDesigner(statusConfig.onCompleted, fields);
                } else {
                    alertPopMessage('danger', 'Error', 'Please upload Final Smile & Final Scan before submitting', 4000, '');
                }
                break;
            default:
                checkoutOrderDesigner(statusConfig.onCompleted, fields);
                break;
        }

    });

    $("#save-checkout-for-finalqc").click(function() {

        if ($("#overallstatusfinal").val() !== null) {
            $('#text-checkout-final').text('');

            var fields = {
                caseno: $("#caseno").val(),
                message: $("#message-text-checkout-finalqc").val() || '',
                qcrating: $("#overallstatusfinal").val() || '',
                assignedTo: $("#assignedtofinal").val() || '',
                rx: $("#rxfinal").val() || '',
                margin: $("#marginfinal").val() || '',
                qdirection: $("#qdirectionalfinal").val() || '',
                contact: $("#contactfinal").val() || '',
                occlusal: $("#occulsalfinal").val() || '',
                shape: $("#shapefinal").val() || '',
                size: $("#sizefinal").val() || '',
            };

            var orderComplete = fields.assignedTo === '' ? statusConfig.onSubmitted : statusConfig.onReturned;

            $('#checkout-for-finalqc').modal('hide');

            var serviceType = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data.serviceType : '0';
            switch (serviceType) {
                case '0':
                    var finalfilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final Scan';
                    });

                    if (finalfilecheck.length > 0) {
                        checkoutOrderFinalqc(orderComplete, fields);
                    } else {
                        alertPopMessage('danger', 'Error', 'Please upload final scan file before submitting', 4000, '');
                    }
                    break;

                case '1':
                    var finalSmilefilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final Smile';
                    });
                    if (finalSmilefilecheck.length > 0) {
                        checkoutOrderFinalqc(orderComplete, fields);
                    } else {
                        alertPopMessage('danger', 'Error', 'Please upload Final Smile before submitting', 4000, '');
                    }
                    break;

                case '2':
                    var finalSmilefilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final Smile';
                    });
                    var finalScanfilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final Scan';
                    });
                    if (finalSmilefilecheck.length > 0 && finalScanfilecheck.length > 0) {
                        checkoutOrderFinalqc(orderComplete, fields);
                    } else {
                        alertPopMessage('danger', 'Error', 'Please upload Final Smile & Final Scan before submitting', 4000, '');
                    }
                    break;

                case '3':
                    var finalStlfilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final STL';
                    });
                    var finalScanfilecheck = allFiles.filter(function(file) {
                        return file.filetype === 'Final Scan';
                    });
                    if (finalStlfilecheck.length > 0 && finalScanfilecheck.length > 0) {
                        checkoutOrderFinalqc(orderComplete, fields);
                    } else {
                        alertPopMessage('danger', 'Error', 'Please upload Final Smile & Final Scan before submitting', 4000, '');
                    }
                    break;

                default:
                    checkoutOrderFinalqc(orderComplete, fields);
                    break;
            }
        } else {
            $('#text-checkout-final').text('*Please select a overall status');
        }
    });

    $("#returnOrd").click(function() {

        if ($("#retReasonDes").val() !== '0') {

            $('#text-return-designer').text('');

            var fields = {
                caseno: $("#caseno").val(),
                message: $("#retReasonDes").val() !== 'Others' ? $("#retReasonDes").val() || '' : $(".message-text-returnDes").val() || '',
                correction: '',
                tool: ''
            };
            checkoutOrderDesigner(statusConfig.onReturned, fields);

            $('#returnModal-designer').modal('hide');
        } else {
            $('#text-return-designer').text('*please select a hold reason');
        }
    });

    $('#searchCase').keypress(function() {

        if ($('#searchCase').val().length > 0) {

            var request = {
                psearchtext: $('#searchCase').val(),
                psearchby: $('#searchBy').val()
            };

            $.ajax({
                url: `${API_URL}getSearchOrderDetails`,
                type: 'POST',
                headers: {
                    sessionid: sessionid
                },
                data: request,
                success: function(result, status, xhr) {
                    autocomplete(document.getElementById('searchCase'), result, $('#searchBy').val());
                },
                error: function(xhr, status, error) {
                    console.log(error);
                }
            });
        }
    });

    //------------------------------------------------------------------------------------------------------------------------------------------------------------------------//
});

//--------------------------------------------------------------No data => clear view to default-------------------------------------------------------------------//

function NoDataAvailable() {

    //main body and save , save next order button to hide by default
    $("#mainContent").css("display", "none");
    $("#saveButton").css("display", "none");

    $(".btn-Save").css('pointer-events', 'none');
    $(".btn-SaveNext").css('pointer-events', 'none');
    $(".btn-Return").css('pointer-events', 'none');
    $(".btn-Save").prop('disabled', true);
    $(".btn-SaveNext").prop('disabled', true);
    $(".btn-Return").prop('disabled', true);

    //timer to disabled by default
    $("#timerCurrent").css('pointer-events', 'none');

    if (messageReload) {
        window.clearInterval(messageReload); // tostop message reloading
    }

    if (tm) {
        window.clearInterval(tm);
    }
    $("#tym").text("00:00:00");
    $("#timerbut").attr("src", "/image/play.png");
    $("#timerbut").removeClass('pause');
    $("#timerbut").addClass('play');
    h = 0; // hours
    m = 0; //minutes
    s = 0; //seconds

    $("#neworderbtn").prop('disabled', false);
    $("#neworderbtn").css('pointer-events', 'auto');

    //Orders on hand text to be Nil
    $("#ordersOnHandNil").text("Order On Hand: Nil");

    //toppanel of content
    $("#caseno").val('');
    $("#lab").val('');
    $("#status").val('');
    $("#receivedOn").val('');
    $("#dueby").val('');
    $("#fileName").val('');

    //All tabs content
    $('#filesDetails').empty();
    $('#caseDetails').empty();
    $('#caseDetailsAdd').empty();
    $('#designPref').empty();

    $("#despCustom").text('');
    $("#despContact").text('');
    $("#despOcclusition").text('');
    $("#despAnatomy").text('');
    $("#despPontic").text('');
    $("#despLiner").text('');

    $('.msg_history').empty();
    $("#myInput").val('');

    $(".custom-spinner").addClass("display-none");
    $("#myFileAttachement").val('');
    $("#filePost").val('')
    $("#myAttachement").val('');
    $("#sendFileAttachement .spanTag").html('Upload');
    $("#sendFileAttachement").attr("disabled", false);

    //for save, return => Values for qc rating, qc reason and comments
    $("#qcratingorder").val('');
    $("#qcreason").val('');
    $(".message-text-save").val('');

    $("#retReasonDes").val('');
    $(".message-text-returnDes").val('');

    $('assignedtoCheckout').hide();

    //Rx details => empty
    $("#designTypeEdit").val('');
    $("#notationsEdit").val('');
    $("#productEdit").val('');
    $("#modelEdit").val('');

    $('.modal-body .modal-data-detailid span').text('');

    $('#assignedtofinal').val(''); //Assigned to field for final qc checkout default to empty

    $('#text-checkout-iosqc').text(''); //Alert inside modal
    $('#text-checkout-designer').text('');
    $('#text-checkout-final').text('');
    $('#text-return-designer').text('');
    clearStorage();

};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//---------------------------------------------------------------Alert popup function--------------------------------------------------------------//
//type => success-green, warning-yellow, danger-red
function alertPopMessage(type, title, message, duration, redirection) {

    $("#alertSucInOrder").remove();
    $("#alertSucOrder").append('<div id="alertSucInOrder" class="alert alert-' + type + ' alertSuccessOrder alert-dismissible fade show" role="alert"><strong>' + title + ': </strong>' + message + '<button type="button" class="close" data-dismiss="alert" aria-label="Close"><span aria-hidden="true">&times;</span></button></div>');
    setTimeout(() => {
        $("#alertSucInOrder").remove();
        if (redirection) {
            window.location.href = redirection;
        }
    }, duration);

};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------Edit Rx details--------------------------------------------------------------------------------//

function rxDetailsDropDown() {

    var defaultDropdownRxdtype = `<option value="0" disabled="">Select Design Type</option>`;
    var defaultDropdownRxprod = `<option value="0" disabled="">Select Product</option>`;
    $("#designTypeEdit").html(defaultDropdownRxdtype);
    $("#productEdit").html(defaultDropdownRxprod);

    const designTypeData = masterData.designtypes;
    const productdata = masterData.products;

    if (designTypeData) {
        $.each(designTypeData, function(key, valDesigntype) {
            $("#designTypeEdit").append(`<option value="${valDesigntype.designtype}">${valDesigntype.designtype}</option>`);
        });
    }
    if (productdata) {
        $.each(productdata, function(key, valProduct) {
            $("#productEdit").append(`<option value="${valProduct.productname}">${valProduct.productname}</option>`);
        });
    }
};
rxDetailsDropDown()

function getRxOrderDetails() {

    var orderid = $("#caseno").val();
    $.ajax({
        url: `${API_URL}getRxOrderDetails?orderId=${orderid}`,
        type: 'POST',
        headers: {
            sessionid: sessionid
        },
        success: function(result, status, xhr) {

            $('#caseDetails').empty();

            if (result[0].ds_ordersid) {

                $.each(result, function(keyCase, valCase) {

                    $('#caseDetails').append(`<tr><td align="center">${keyCase + 1}</td><td align="left">${valCase.designtype}</td><td align="left">${valCase.notations}</td><td align="left">${valCase.product}</td><td align="left">${valCase.model}</td><td align="center"><span id="btn_${keyCase}"></span></td></tr>`);

                    var buttonCaseEdit = document.createElement('button');
                    buttonCaseEdit.type = "button";
                    buttonCaseEdit.id = `btn_icon${keyCase}`;
                    buttonCaseEdit.className = "button-add-edit-case btn btn-primary";
                    buttonCaseEdit.addEventListener('click', function() {
                        editRxDetails(keyCase, valCase);
                    });

                    $(`#btn_${keyCase}`).append(buttonCaseEdit);
                    $(`#btn_icon${keyCase}`).append('<i class="fa fa-edit"></i>');
                });
            }
        },
        error: function(xhr, status, error) {
            console.log(error);
        }
    });
};

function editRxDetails(key, value) {

    $("#editCaseDetails").modal('show')

    $("#designTypeEdit").val(value.designtype);
    $("#notationsEdit").val(value.notations);
    $("#productEdit").val(value.product);
    $("#modelEdit").val(value.model);

    $('.modal-body .modal-data-detailid').html(`<span style="display:none">${value.ds_rxdetailid}</span>`);
    $('.modal-body .modal-data-source span').text('edit');
};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//------------------------------------------------------------------Construct data from session storage---------------------------------------------------------------------------------------------------//

function constructNewOrderData(order, status) {

    $("#mainContent").css("display", "block"); // show the content on successfull pull order
    $("#saveButton").css("display", "flex"); // show the save button on successfull pull order

    $("#timerCurrent").css('pointer-events', 'auto'); //making timer enable

    $("#neworderbtn").prop('disabled', true);
    $("#neworderbtn").css('pointer-events', 'none'); // new order button disabled and disable poiter events

    $("#ordersOnHandNil").text("Order On Hand"); //Orders on hand text to change back to empty

    orderData = order.result[0].data;

    //case details to fill up
    $("#caseno").val(`${orderData.caseno}`);
    $("#lab").val(`${orderData.customer}`);
    $("#status").val(status);
    $("#receivedOn").val(`${orderData.receivedOn}`);
    $("#dueby").val(`${orderData.dueBy}`);
    $("#fileName").val(`${orderData.fileName}`);

    timerStore = sessionStorage.getItem('TimerStore');
    if (timerStore) {
        var temp = JSON.parse(timerStore);
        // var split = temp.value.split(':');
        // console.log(`${split[0]}:${split[1]}:${split[2]}`);

        h = parseInt(temp.h, 10);
        m = parseInt(temp.m);
        s = parseInt(temp.s);
        $("#tym").text(temp.value);
    }
    timerStatusStore = sessionStorage.getItem('TimerStatusStore');
    if (timerStatusStore) {
        toggleTimer(timerStatusStore);
    } else {
        toggleTimer('pause');
    }

    getRxOrderDetails(); // Case Details

    fileTableRender(); //file tab
    if (userDetails.ugroup === 'Designer') {
        var serviceType = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data.serviceType : '0';
        switch (serviceType) {
            case '0':
                $('#final-file-option').hide();
                $('#filetype-easysmile').hide();
                $('#filetype-easyvalidate').hide();
                break;
            case '1':
                $('#final-file-option').hide();
                $('#filetype-easysmile').show();
                $('#filetype-easyvalidate').hide();
                break;
            case '2':
                $('#final-file-option').show();
                $('#filetype-easysmile').show();
                $('#filetype-easyvalidate').hide();
                break;
            case '3':
                $('#final-file-option').show();
                $('#filetype-easysmile').hide();
                $('#filetype-easyvalidate').show();
                break;
            default:
                $('#final-file-option').show();
                break;
        }
    }

    $('#caseDetailsAdd').html('<tr><td><button class="button-add-edit-case btn btn-primary" type="button" data-toggle="modal" data-target="#editCaseDetails" data-src="add"><i class="fa fa-plus-circle" aria-hidden="true"></i></button></td></tr>')

    // var desiPrefTab = $('#designPref');
    if (orderData.designPref && orderData.designPref.length > 0) {
        $("#despCustom").text(`${orderData.designPref[0].custom !== null || undefined || '' ? orderData.designPref[0].custom : '-'}`);
        $("#despContact").text(`${orderData.designPref[0].contact !== null || undefined || '' ? orderData.designPref[0].contact : '-'}`);
        $("#despOcclusition").text(`${orderData.designPref[0].occlusion !== null || undefined || '' ? orderData.designPref[0].occlusion : '-'}`);
        $("#despAnatomy").text(`${orderData.designPref[0].anatomy !== null || undefined || '' ? orderData.designPref[0].anatomy : '-'}`);
        $("#despPontic").text(`${orderData.designPref[0].ponitic !== null || undefined || '' ? orderData.designPref[0].ponitic : '-'}`);
        $("#despLiner").text(`${orderData.designPref[0].linerSpacer !== null || undefined || '' ? orderData.designPref[0].linerSpacer : '-'}`);
        // $.each(order.designPref, function(key, val) {
        //     // });
    } else {
        $("#despCustom").text('-');
        $("#despContact").text('-');
        $("#despOcclusition").text('-');
        $("#despAnatomy").text('-');
        $("#despPontic").text('-');
        $("#despLiner").text('-');
    }
    getMessage();

    if (messageReload) {
        window.clearInterval(messageReload); // tostop message reloading
    }

    messageReload = window.setInterval('getMessage()', 8000);
    scrollToBottom();

    $('#assignedtofinal').val(''); //Assigned to field for final qc checkout default to empty

    $('#text-checkout-iosqc').text(''); //Alert inside modal
    $('#text-checkout-designer').text('');
    $('#text-checkout-final').text('');
    $('#text-return-designer').text('');

};

function constructViewOrder(order, status) {

    $("#mainContent").css("display", "block"); // show the content on successfull pull order
    $("#saveButton").css("display", "none"); // show the save button on successfull pull order

    $("#timerCurrent").css('pointer-events', 'none'); //making timer enable

    $("#neworderbtn").prop('disabled', false);
    $("#neworderbtn").css('pointer-events', 'auto'); // new order button disabled and disable poiter events

    $("#ordersOnHandNil").text("Order On Hand"); //Orders on hand text to change back to empty

    orderData = order.result[0].data;

    //case details to fill up
    $("#caseno").val(`${orderData.caseno}`);
    $("#lab").val(`${orderData.customer}`);
    $("#status").val(status);
    $("#receivedOn").val(`${orderData.receivedOn}`);
    $("#dueby").val(`${orderData.dueBy}`);
    $("#fileName").val(`${orderData.fileName}`);

    timerStore = sessionStorage.getItem('TimerStore');
    if (timerStore) {
        var temp = JSON.parse(timerStore);
        // var split = temp.value.split(':');
        // console.log(`${split[0]}:${split[1]}:${split[2]}`);

        h = parseInt(temp.h, 10);
        m = parseInt(temp.m);
        s = parseInt(temp.s);
        $("#tym").text(temp.value);
    }
    timerStatusStore = sessionStorage.getItem('TimerStatusStore');
    if (timerStatusStore) {
        toggleTimer(timerStatusStore);
    } else {
        toggleTimer('pause');
    }

    getRxOrderDetails(); // Case Details

    fileTableRender(); //file tab
    if (userDetails.ugroup === 'Designer') {
        var serviceType = sessionStorage.getItem('orderOnHand') ? JSON.parse(sessionStorage.getItem('orderOnHand')).result[0].data.serviceType : '0';
        switch (serviceType) {
            case '0':
                $('#final-file-option').hide();
                $('#filetype-easysmile').hide();
                $('#filetype-easyvalidate').hide();
                break;
            case '1':
                $('#final-file-option').hide();
                $('#filetype-easysmile').show();
                $('#filetype-easyvalidate').hide();
                break;
            case '2':
                $('#final-file-option').show();
                $('#filetype-easysmile').show();
                $('#filetype-easyvalidate').hide();
                break;
            case '3':
                $('#final-file-option').show();
                $('#filetype-easysmile').hide();
                $('#filetype-easyvalidate').show();
                break;
            default:
                $('#final-file-option').show();
                break;
        }
    }

    $('#caseDetailsAdd').html('<tr><td><button class="button-add-edit-case btn btn-primary" type="button" data-toggle="modal" data-target="#editCaseDetails" data-src="add"><i class="fa fa-plus-circle" aria-hidden="true"></i></button></td></tr>')

    // var desiPrefTab = $('#designPref');
    if (orderData.designPref && orderData.designPref.length > 0) {
        $("#despCustom").text(`${orderData.designPref[0].custom !== null || undefined || '' ? orderData.designPref[0].custom : '-'}`);
        $("#despContact").text(`${orderData.designPref[0].contact !== null || undefined || '' ? orderData.designPref[0].contact : '-'}`);
        $("#despOcclusition").text(`${orderData.designPref[0].occlusion !== null || undefined || '' ? orderData.designPref[0].occlusion : '-'}`);
        $("#despAnatomy").text(`${orderData.designPref[0].anatomy !== null || undefined || '' ? orderData.designPref[0].anatomy : '-'}`);
        $("#despPontic").text(`${orderData.designPref[0].ponitic !== null || undefined || '' ? orderData.designPref[0].ponitic : '-'}`);
        $("#despLiner").text(`${orderData.designPref[0].linerSpacer !== null || undefined || '' ? orderData.designPref[0].linerSpacer : '-'}`);
        // $.each(order.designPref, function(key, val) {
        //     // });
    } else {
        $("#despCustom").text('-');
        $("#despContact").text('-');
        $("#despOcclusition").text('-');
        $("#despAnatomy").text('-');
        $("#despPontic").text('-');
        $("#despLiner").text('-');
    }
    getMessage();

    if (messageReload) {
        window.clearInterval(messageReload); // tostop message reloading
    }

    messageReload = window.setInterval('getMessage()', 8000);
    scrollToBottom();

    $('#assignedtofinal').val(''); //Assigned to field for final qc checkout default to empty

    $('#text-checkout-iosqc').text(''); //Alert inside modal
    $('#text-checkout-designer').text('');
    $('#text-checkout-final').text('');
    $('#text-return-designer').text('');

};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------Checkout and qc reason----------------------------------------------------------------------

var iosqureason = function(qcreasonsdropdown) {

    var defaultDropdown = `<option selected="" value="0">Select the reason</option>`;
    qcreasonsdropdown.html(defaultDropdown);
    const reasonData = masterData.reasons;

    if (reasonData) {
        $.each(reasonData, function(keyqcreasons, valCase) {
            qcreasonsdropdown.append(`<option value="${valCase.reason}">${valCase.reason}</option>`);
        });
    }
};

function checkoutOrderIosqc(statusCode, fields) {

    var details = {
        caseno: fields.caseno,
        status: statusCode,
        comments: fields.message || '',
        updatedby: userDetails.USERNAME,
        updatedon: '',
        qcrating: fields.qcrating || '',
        qcreason: fields.qcreason || ''
    }

    $.ajax({
        url: '/updateStatusIosqc',
        type: 'POST',
        data: details,
        headers: {
            'x-access-token': apiToken
        },
        success: function(data, status, xhr) {

            alertPopMessage('success', 'Success', statusText[statusCode], 4000, '');
            NoDataAvailable();
            $('#neworderbtn').click();

        },
        error: function(xhr, status, error) {

            if (xhr.status === 401) {

                var messageUnathorized = 'Unauthorized! session expired, Please login again';
                alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

            } else {

                var messageError = xhr.responseText;
                alertPopMessage('danger', 'Error', messageError, 4000, '');

            }
        }
    });
};

function checkoutOrderDesigner(statusCode, fields) {

    var details = {
        caseno: fields.caseno,
        status: statusCode,
        comments: fields.message || '',
        updatedby: userDetails.USERNAME,
        updatedon: '',
        correction: fields.correction || '',
        tool: fields.tool || ''
    }

    $.ajax({
        url: '/updateStatusDesigner',
        type: 'POST',
        data: details,
        headers: {
            'x-access-token': apiToken
        },
        success: function(data, status, xhr) {

            alertPopMessage('success', 'Success', statusText[statusCode], 4000, '');
            NoDataAvailable();
            $('#neworderbtn').click();

        },
        error: function(xhr, status, error) {

            if (xhr.status === 401) {

                var messageUnathorized = 'Unauthorized! session expired, Please login again';
                alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

            } else {

                var messageError = xhr.responseText;
                alertPopMessage('danger', 'Error', messageError, 4000, '');

            }
        }
    });
};

function checkoutOrderFinalqc(statusCode, fields) {

    var details = {
        caseno: fields.caseno,
        status: statusCode,
        comments: fields.message || '',
        updatedby: userDetails.USERNAME,
        updatedon: '',
        qcrating: fields.qcrating || '',
        assignedTo: fields.assignedTo || '',
        rx: fields.rx || '',
        margin: fields.margin || '',
        qdirection: fields.qdirection || '',
        contact: fields.contact || '',
        occlusal: fields.occlusal || '',
        shape: fields.shape || '',
        size: fields.size || '',

    }

    $.ajax({
        url: '/updateStatusFinalQc',
        type: 'POST',
        data: details,
        headers: {
            'x-access-token': apiToken
        },
        success: function(data, status, xhr) {

            alertPopMessage('success', 'Success', statusText[statusCode], 4000, '');
            NoDataAvailable();
            $('#neworderbtn').click();

        },
        error: function(xhr, status, error) {

            if (xhr.status === 401) {

                var messageUnathorized = 'Unauthorized! session expired, Please login again';
                alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

            } else {

                var messageError = xhr.responseText;
                alertPopMessage('danger', 'Error', messageError, 4000, '');

            }
        }
    });
};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//-----------------------------------------------------------------update status by toggling the start/pause------------------------------------------------------------//

function toggle(el) {
    if (el.className != "pause") {

        updateOrderStatus(statusConfig.onStarted, el);

    } else if (el.className == "pause") {

        updateOrderStatus(statusConfig.onPaused, el);
    }

    return false;
};

var updateOrderStatus = function(statusCode, actions) {

    var details = {
        caseno: $("#caseno").val(),
        status: statusCode,
        remarks: '',
        updatedby: userDetails.USERNAME,
        updatedon: ''
    };

    $.ajax({
        url: '/updateStatus',
        type: 'POST',
        data: details,
        headers: {
            'x-access-token': apiToken
        },
        success: function(data, status, xhr) {

            orderStatusStore = statusText[statusCode];
            sessionStorage.setItem('OrderStatusStore', orderStatusStore);

            alertPopMessage('success', 'Success', statusText[statusCode], 4000, '');

            $("#status").val(statusText[statusCode]);
            toggleTimer(actions.className);
        },
        error: function(xhr, status, error) {

            if (xhr.status === 401) {

                var messageUnathorized = 'Unauthorized! session expired, Please login again';
                alertPopMessage('danger', 'Error', messageUnathorized, 4000, '/login');

            } else {

                var messageError = xhr.responseText;
                alertPopMessage('danger', 'Error', messageError, 4000, '');

            }
        }
    });

};

var toggleTimer = function(className) {

    if (className !== 'pause') {
        tm = window.setInterval('disp()', 1000);

        $("#timerbut").attr("src", "/image/pause.png");
        $("#timerbut").removeClass('play');
        $("#timerbut").addClass('pause');

        timerStatusStore = 'play';
        sessionStorage.setItem('TimerStatusStore', timerStatusStore);

        $("#saveButton").css('pointer-events', 'auto');

        $(".btn-Save").css('pointer-events', 'auto');
        $(".btn-SaveNext").css('pointer-events', 'auto');
        $(".btn-Return").css('pointer-events', 'auto');
        $(".btn-Save").prop('disabled', false);
        $(".btn-SaveNext").prop('disabled', false);
        $(".btn-Return").prop('disabled', false);
    } else {
        $("#saveButton").css('pointer-events', 'none');

        $(".btn-Save").css('pointer-events', 'none');
        $(".btn-SaveNext").css('pointer-events', 'none');
        $(".btn-Return").css('pointer-events', 'none');
        $(".btn-Save").prop('disabled', true);
        $(".btn-SaveNext").prop('disabled', true);
        $(".btn-Return").prop('disabled', true);

        if (tm) {
            window.clearInterval(tm);
        } // stop the timer
        $("#timerbut").attr("src", "/image/play.png");
        $("#timerbut").removeClass('pause');
        $("#timerbut").addClass('play');

        timerStatusStore = 'pause';
        sessionStorage.setItem('TimerStatusStore', timerStatusStore);
    }
};

function disp() {
    // Format the output by adding 0 if it is single digit //
    if (s < 10) { var s1 = '0' + s; } else { var s1 = s; }
    if (m < 10) { var m1 = '0' + m; } else { var m1 = m; }
    if (h < 10) { var h1 = '0' + h; } else { var h1 = h; }
    // Display the output //
    str = h1 + ':' + m1 + ':' + s1;
    // document.getElementById('n1').innerHTML = str;
    document.getElementById('tym').innerHTML = str;
    timerStore = {
        value: str,
        h: h1,
        m: m1,
        s: s1
    };
    sessionStorage.setItem('TimerStore', JSON.stringify(timerStore));
    // Calculate the stop watch //
    if (s < 59) {
        s = s + 1;
    } else {
        s = 0;
        m = m + 1;
        if (m == 60) {
            m = 0;
            h = h + 1;
        } // end if  m ==60
    } // end if else s < 59
    // end of calculation for next display
};
//----------------------------------------------------------------------------------------------------------------------------------------------------------------//

//--------------------------------------------------------------------Messages------------------------------------------------------------------------------//

function messageRender(txtMsg, fileAttachments) {

    caseNo = $("#caseno").val();

    var messages = {
        columns: {
            messagetype: "0",
            message: txtMsg,
            caseno: caseNo,
            refmessageid: ""
        },
        attachments: fileAttachments
    };

    $.ajax({
        url: `${API_URL}sendMessage`,
        type: 'POST',
        data: JSON.stringify(messages),
        dataType: 'json',
        async: false,
        headers: {
            sessionid: sessionid,
            'Content-Type': 'application/json'
        },
        success: function(data) {

            $("#myInput").val('');
            getMessage();
            $("#sendAttachmentIcon-spinner").addClass("display-none");
            $("#submitMessage").removeClass("display-none");
            return true;
        },
        error: function() {
            console.log("error");
            $("#sendAttachmentIcon-spinner").addClass("display-none");
            $("#submitMessage").removeClass("display-none");
            return false;
        }
    });
    // })
};

function getMessage() {
    caseNo = $("#caseno").val();
    var user = userDetails.USERNAME;
    if (user && caseNo) {
        $.ajax({
            url: `${API_URL}getOrderMessage?messageType=0&pid=${caseNo}`,
            type: 'POST',
            headers: {
                sessionid: sessionid,
                'Content-Type': 'application/json'
            },
            success: function(data, status) {
                $('.msg_history').empty();
                // let user = "user";
                for (let item of data) {
                    if (item.messageid !== '') {
                        if (user === item.mfrom) {
                            $('.msg_history').append(`
                            <div class="outgoing_msg">
                            <div class="sent_msg" id="sendMessage_${item.rowno}">
                            <p class="msg_position_left">
                                             ${item.message}
                                            </p>
                                    
                                            <span class="time_date">
                                                ${item.createdon}</span
                                            >
                                            
                                    </div>
                                    </div>
                                    `)
                        } else {
                            $('.msg_history').append(`
                                                    <div class="incoming_msg">
                                                    <div class="incoming_msg_img">
                                                    <img
                                    src="https://ptetutorials.com/images/user-profile.png"
                                    alt="sunil"
                                />
                            </div>
                            <div class="received_msg">
                            <div class="received_withd_msg" id="resiveMsg_${item.rowno}">
                                    <p>
                                     ${item.message}
                                     </p>
                                     <span class="time_date">
                                        ${item.createdon} - By ${item.mfrom}</span
                                    >
                                </div>
                                
                            </div>
                            </div>
                            `)
                        }
                        if (item.attachmentname !== '') {
                            var ielement = document.createElement('div');
                            // ielement.innerHTML = item.attachmentname;
                            ielement.innerHTML = `${item.attachmentname}<i class='fa fa-download download_icon_attachment'></i>`;
                            ielement.className = "fileAttachementDownload";
                            ielement.addEventListener('click', function() {
                                downloadFile(item.attachfilename);
                            });
                            if (user === item.mfrom) {
                                $(`#sendMessage_${item.rowno}`).append(ielement);
                            } else {
                                $(`#resiveMsg_${item.rowno}`).append(ielement);
                            }
                        }
                    }
                }
                return true;
            },
            error: function() {
                console.log("error");
                return false;
            }
        })
    }
    // return scrollToBottom();
}

function scrollToBottom() {
    $('#scrollHeight').animate({ scrollTop: $('#scrollHeight')[0].scrollHeight }, 1000);
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//------------------------------------------------------------file attachment and download----------------------------------------------------------------//

function fileTableRender() {

    caseNo = $("#caseno").val();

    $.ajax({
        url: `${API_URL}getFiles?orderId=${caseNo}`,
        type: 'GET',
        headers: {
            sessionid: sessionid,
            'Content-Type': 'application/json'
        },
        success: function(response) {

            $('#filesDetails').empty();
            allFiles = response;
            for (let item of response) {
                var fileName = item.filename.split('/');
                if (item.filetype === ('STL File' || 'Initial STL' || 'Final STL')) {
                    $('#filesDetails').append(`<tr><td>${item.rowno}</td><td align="left">${item.filetype}</td><td align="left">${fileName[fileName.length - 1]}</td><td><span id="btn_down_${item.rowno}"></span><i id="stlviewing" class="fa fa-eye" aria-hidden="true"></i></td></tr>`);
                } else {
                    $('#filesDetails').append(`<tr><td>${item.rowno}</td><td align="left">${item.filetype}</td><td align="left">${fileName[fileName.length - 1]}</td><td><span id="btn_down_${item.rowno}"></span></td></tr>`);
                }
                var ielement = document.createElement('i');
                ielement.className = "fa fa-download download_icon";
                ielement.addEventListener('click', function() {
                    downloadFile(item.filename);
                });
                $(`#btn_down_${item.rowno}`).append(ielement);
                // <i class="fa fa-download" onclick="downloadFile(${item})"></i>
            }

        },
        error: function(error) {
            console.log(error)
        }
    })
};

function messageFileAttachement(input, attachments) {
    caseNo = $("#caseno").val();
    var fd = new FormData();
    var files = $('#myAttachment')[0].files[0];
    fd.append('filetoattach', files);

    var message = input || `PFA - ${files.name}`;
    // input === '' ? $('#myInput').val(files.name) : input;
    // to-do send msg default 
    if ($('#myAttachment').val()) {
        $.ajax({
            url: `${API_URL}attachMessageFile`,
            type: 'POST',
            data: fd,
            contentType: false,
            processData: false,
            headers: {
                orderid: caseNo,
                sourcetype: 'Message',
                dsorderid: sessionStorage.getItem('dsordersid'),
                sessionid: sessionid
            },
            success: function(response) {
                // console.log(isFile);
                // var attachments = [];
                $('#myAttachment').val('');
                if (response.status) {
                    attachments.push({
                        attachmentname: response.fileName,
                        attachfilename: `${response.location}/${response.fileName}`
                    })

                    messageRender(message, attachments);
                }
            },
            error: function(error) {
                $('#myAttachment').val('');
                return error;
            }
        });
    }
}

function downloadFile(data) {

    $.ajax({
        url: `${API_URL}getDownloadUrl?fullPath=${data}`,
        type: 'GET',
        success: function(response) {

            window.open(response.signedUrl);
        },
        error: function(error) {
            console.log(error)
        }
    })
};
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------Search input----------------------------------------------------------------------//

function autocomplete(inp, arr, searchBy) {
    var resultArr;
    /*the autocomplete function takes two arguments,
    the text field element and an array of possible autocompleted values:*/
    var currentFocus;
    /*execute a function when someone writes in the text field:*/
    inp.addEventListener("input", function(e) {
        var a, b, i, val = this.value;
        /*close any already open lists of autocompleted values*/
        closeAllLists();
        if (!val) { return false; }
        currentFocus = -1;
        /*create a DIV element that will contain the items (values):*/
        a = document.createElement("DIV");
        a.setAttribute("id", this.id + "autocomplete-list");
        a.setAttribute("class", "autocomplete-items");
        /*append the DIV element as a child of the autocomplete container:*/
        this.parentNode.appendChild(a);
        /*for each item in the array...*/
        for (i = 0; i < arr.length; i++) {
            if (searchBy === 'Order No') {
                resultArr = arr[i].sresult;
            } else {
                resultArr = arr[i].sresult.split('- ')[1];
            }
            /*check if the item starts with the same letters as the text field value:*/
            if (resultArr.substr(0, val.length).toUpperCase() == val.toUpperCase()) {
                /*create a DIV element for each matching element:*/
                b = document.createElement("DIV");
                /*make the matching letters bold:*/
                if (searchBy === 'Order No') {
                    b.innerHTML = "<strong>" + resultArr.substr(0, val.length) + "</strong>";
                    b.innerHTML += resultArr.substr(val.length);
                } else {
                    b.innerHTML = arr[i].sresult.split('- ')[0];
                    b.innerHTML += "- <strong>" + resultArr.substr(0, val.length) + "</strong>";
                    b.innerHTML += resultArr.substr(val.length);
                }
                /*insert a input field that will hold the current array item's value:*/
                b.innerHTML += "<input type='hidden' value='" + arr[i].sresult + "'>";
                /*execute a function when someone clicks on the item value (DIV element):*/
                b.addEventListener("click", function(e) {
                    /*insert the value for the autocomplete text field:*/
                    inp.value = this.getElementsByTagName("input")[0].value;
                    /*close the list of autocompleted values,
                    (or any other open lists of autocompleted values:*/
                    closeAllLists();
                });
                a.appendChild(b);
            }
        }
    });
    /*execute a function presses a key on the keyboard:*/
    inp.addEventListener("keydown", function(e) {
        var x = document.getElementById(this.id + "autocomplete-list");
        if (x) x = x.getElementsByTagName("div");
        if (e.keyCode == 40) {
            /*If the arrow DOWN key is pressed,
            increase the currentFocus variable:*/
            currentFocus++;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 38) { //up
            /*If the arrow UP key is pressed,
            decrease the currentFocus variable:*/
            currentFocus--;
            /*and and make the current item more visible:*/
            addActive(x);
        } else if (e.keyCode == 13) {
            /*If the ENTER key is pressed, prevent the form from being submitted,*/
            e.preventDefault();
            if (currentFocus > -1) {
                /*and simulate a click on the "active" item:*/
                if (x) x[currentFocus].click();
            }
        }
    });

    function addActive(x) {
        /*a function to classify an item as "active":*/
        if (!x) return false;
        /*start by removing the "active" class on all items:*/
        removeActive(x);
        if (currentFocus >= x.length) currentFocus = 0;
        if (currentFocus < 0) currentFocus = (x.length - 1);
        /*add class "autocomplete-active":*/
        x[currentFocus].classList.add("autocomplete-active");
    }

    function removeActive(x) {
        /*a function to remove the "active" class from all autocomplete items:*/
        for (var i = 0; i < x.length; i++) {
            x[i].classList.remove("autocomplete-active");
        }
    }

    function closeAllLists(elmnt) {
        /*close all autocomplete lists in the document,
        except the one passed as an argument:*/
        var x = document.getElementsByClassName("autocomplete-items");
        for (var i = 0; i < x.length; i++) {
            if (elmnt != x[i] && elmnt != inp) {
                x[i].parentNode.removeChild(x[i]);
            }
        }
    }
    /*execute a function when someone clicks in the document:*/
    document.addEventListener("click", function(e) {
        closeAllLists(e.target);
    });
}
//------------------------------------------------------------------------------------------------------------------------------------------------------------//

//----------------------------------------------------------------Clear storage from session storage---------------------------------------------------------------//
function clearStorage() {
    if (sessionStorage.getItem('orderOnHand')) {
        sessionStorage.removeItem('orderOnHand');
        sessionStorage.removeItem('TimerStore', timerStore);
        sessionStorage.removeItem('TimerStatusStore', timerStatusStore);
        sessionStorage.removeItem('OrderStatusStore', orderStatusStore);
    }
};
//------------------------------------------------------------------------------------------------------------------------------------------------------------//