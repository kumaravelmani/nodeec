//-----------------------------Widgets data -------------------------------

$(document).ready(function() {
    var WidgetReload = function() {

        var username = JSON.parse(sessionStorage.getItem('UserDetails')).USERNAME;

        $.ajax({
            type: 'GET',
            url: `/getWidgetData?role=${username}`,
            headers: {
                'x-access-token': sessionStorage.getItem('token')
            },
            success: function(result) {

                var data = JSON.parse(result);
                var widgetData = data.result[0].data;

                var names = Object.keys(widgetData);
                var values = Object.values(widgetData);

                // var data = resultData.po_result;
                // var widgetDataObject = JSON.stringify(widgetData);
                // console.log(`Widget Data :: ${widgetDataObject} ==> ${widgetDataObject.indexOf('errorNum')}`);
                // if (widgetDataObject.indexOf('errorNum') === -1) {
                //     var response = widgetData.split(',');
                var i;
                var colors = ['bg-primary', 'bg-warning', 'bg-success', 'bg-danger'];
                $('.widget').empty();
                for (i = 0; i < names.length; i++) {
                    for (i = 0; i < values.length; i++) {
                        for (i = 0; i < colors.length; i++) {
                            if (names[i] !== '' && names[i] !== ' ' && values[i] !== '' && values[i] !== ' ') {
                                $('.widget').append(`<div class="col-xl-3 col-sm-6 mb-3">
                            <div class="card text-white ${colors[i]} o-hidden h-100">
                            <div class="card-body">
                                <div class="card-body-icon"></div>
                                <p class="bold font-weight-bold font_size" id="widget1n">${values[i]}</p>
                                <div id="widget1v">${names[i]}</div>
                            </div>
                        </div>
                        </div>`)
                            }
                        }
                    }
                }
            },
            error: function(xhr, status, error) {
                var errorMessage = xhr.status + ': ' + xhr.statusText
                console.log(`${errorMessage}`);
            }
        });

    };
    WidgetReload();
    var interval = 5000;
    // var interval = 360000;
    setInterval(WidgetReload, interval);
});